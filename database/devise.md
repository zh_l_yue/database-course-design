# 数据库设计：<br>

***

## 触发器：

#### insert型触发器
1. xxx


## 级联实现

#### delete
1. room删除后删除rc表内的安排（没有该教室就没有该安排）
2. teacher删除后删除tc表、rc表内的安排
3. course删除后删除tc表、rc表内的安排（没有该课程老师就不需要教学该课）
4. XXX

#### update
1. course表的Cno更新后rc表、tc表的Cno也更新
2. teacher表中的Tno更新后。。。
3. room表的Rno更新后。。
4. XXX


***
- 附数据库的er图：
  ![数据库的er图](img/er.png)
