/*
 Navicat Premium Data Transfer

 Source Server         : conn-localhost
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : university2

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 18/04/2024 22:54:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `Cno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Cname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Ccredit` int NULL DEFAULT NULL,
  PRIMARY KEY (`Cno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('C001', '思想道德修养与法律基础', 3);
INSERT INTO `course` VALUES ('C002', '近现代史纲要', 3);
INSERT INTO `course` VALUES ('C003', '马克思主义原理', 2);
INSERT INTO `course` VALUES ('C004', '高等数学', 3);
INSERT INTO `course` VALUES ('C005', '线性代数', 4);
INSERT INTO `course` VALUES ('C006', '概率统计', 4);
INSERT INTO `course` VALUES ('C007', '大学物理', 2);
INSERT INTO `course` VALUES ('C008', '大学体育', 3);
INSERT INTO `course` VALUES ('C009', '大学英语', 2);
INSERT INTO `course` VALUES ('C010', '计算机基础', 4);
INSERT INTO `course` VALUES ('C011', '军事训练', 2);
INSERT INTO `course` VALUES ('C012', '中国近代史', 1);
INSERT INTO `course` VALUES ('C013', '毛概', 2);

-- ----------------------------
-- Table structure for rc
-- ----------------------------
DROP TABLE IF EXISTS `rc`;
CREATE TABLE `rc`  (
  `Rno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Cno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Tno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `UStime` datetime NULL DEFAULT NULL,
  `UEtime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`Rno`, `Cno`, `Tno`) USING BTREE,
  INDEX `Cno`(`Cno`) USING BTREE,
  INDEX `rc_ibfk_3`(`Tno`) USING BTREE,
  CONSTRAINT `rc_ibfk_1` FOREIGN KEY (`Rno`) REFERENCES `room` (`Rno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rc_ibfk_2` FOREIGN KEY (`Cno`) REFERENCES `course` (`Cno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rc_ibfk_3` FOREIGN KEY (`Tno`) REFERENCES `teacher` (`Tno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rc
-- ----------------------------
INSERT INTO `rc` VALUES ('R101', 'C001', 'T002', '2024-04-12 14:38:16', '2024-04-12 14:38:16');
INSERT INTO `rc` VALUES ('R101', 'C002', 'T002', '2024-04-12 15:56:52', '2024-04-01 15:56:52');
INSERT INTO `rc` VALUES ('R101', 'C003', 'T001', '2024-04-12 14:37:54', '2024-04-12 14:37:54');
INSERT INTO `rc` VALUES ('R101', 'C003', 'T005', '2024-04-12 14:37:26', '2024-04-12 14:37:26');
INSERT INTO `rc` VALUES ('R102', 'C002', 'T002', '2024-04-08 18:15:17', '2024-04-08 18:15:20');

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `Rno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Rcapacity` int NULL DEFAULT NULL,
  `Rlocation` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Requipment` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RStime` datetime NULL DEFAULT NULL,
  `REtime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`Rno`) USING BTREE,
  INDEX `Rno`(`Rno`) USING BTREE,
  INDEX `Rno_2`(`Rno`) USING BTREE,
  INDEX `Rno_3`(`Rno`) USING BTREE,
  INDEX `Rno_4`(`Rno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of room
-- ----------------------------
INSERT INTO `room` VALUES ('R101', 30, 'Building A Room 101', 'Projector, Whiteboard', '2024-04-02 08:00:00', '2024-04-02 10:00:00');
INSERT INTO `room` VALUES ('R102', 25, 'Building A Room 102', 'Whiteboard', '2024-04-02 10:00:00', '2024-04-02 12:00:00');
INSERT INTO `room` VALUES ('R103', 40, 'Building B Room 103', 'Projector, Whiteboard', '2024-04-02 08:00:00', '2024-04-02 12:00:00');
INSERT INTO `room` VALUES ('R104', 35, 'Building C Room 104', 'Projector, Computer', '2024-04-03 13:00:00', '2024-04-03 15:00:00');
INSERT INTO `room` VALUES ('R105', 20, 'Building D Room 105', 'Whiteboard, Printer', '2024-04-03 10:00:00', '2024-04-03 12:00:00');
INSERT INTO `room` VALUES ('R110', 76, 'KZNGECBPCu', 'XZF6QKVOI5', '2024-02-26 23:05:10', '2015-11-23 16:26:46');
INSERT INTO `room` VALUES ('R111', 166, 'b59lBP25Jf', '132BUWI0Gl', '2024-02-08 03:35:47', '2015-08-22 23:21:36');
INSERT INTO `room` VALUES ('R112', 172, 'WSRX3mPs9a', 'FjpXtASb9f', '2024-02-24 00:16:28', '2004-04-06 21:08:36');
INSERT INTO `room` VALUES ('R120', 191, 'z3FCqT9zjD', 'svPGbORGHg', '2024-05-24 19:01:20', '2019-06-26 14:31:07');
INSERT INTO `room` VALUES ('R123', 1111, '123', '1', '2024-04-18 22:07:52', '2024-04-18 22:07:52');
INSERT INTO `room` VALUES ('R130', 135, 'WHo4v4BY2A', 'fNwfkToD0Y', '2024-11-29 09:38:22', '2007-10-30 12:05:08');
INSERT INTO `room` VALUES ('R211', 111, 'Hjlg3U2Np0', 'bpvnWPVrWe', '2024-03-08 00:53:09', '2021-07-09 07:20:28');
INSERT INTO `room` VALUES ('R213', 168, 'Glv6XO0Tar', 'MzgxIr7rW5', '2024-01-11 07:18:59', '2004-01-16 14:20:28');
INSERT INTO `room` VALUES ('R214', 195, 'xHL5qGJjHm', 'NJvGD26gGV', '2024-09-21 14:49:16', '2022-04-05 14:40:10');
INSERT INTO `room` VALUES ('R215', 124, 'lTtfeZ9706', 'r2LHxDy975', '2024-09-18 10:51:18', '2014-11-28 01:24:08');
INSERT INTO `room` VALUES ('R216', 148, 'ykmmWmqEFl', 'mQRsyuhjlu', '2024-11-10 10:54:28', '2023-08-28 12:00:43');
INSERT INTO `room` VALUES ('R217', 60, 'g2ocLtBaFk', 'fFkkNexoZD', '2024-07-13 10:02:23', '2005-04-07 04:58:35');
INSERT INTO `room` VALUES ('R218', 119, '4iYR9JPD2M', '3RPtyOv4Cd', '2024-05-29 08:38:21', '2009-01-16 12:21:29');
INSERT INTO `room` VALUES ('R219', 124, 'g0tiy5hXks', 'CaWMQJ3mfQ', '2024-08-13 03:06:28', '2016-01-01 03:21:16');
INSERT INTO `room` VALUES ('R220', 116, 'qRg3mF4YNz', 'hkFHFovZZQ', '2024-07-08 02:06:27', '2015-01-18 18:23:37');
INSERT INTO `room` VALUES ('R410', 71, 'kln91gcaMQ', 'PWvwuiaNr8', '2024-12-03 02:40:21', '2005-07-16 13:00:43');
INSERT INTO `room` VALUES ('R501', 118, 'uPjr8Kn95F', '42G0o3YAjQ', '2024-07-19 18:10:57', '2010-11-12 10:31:47');
INSERT INTO `room` VALUES ('R601', 71, 'V9kNGRGHVX', 'R1j7MCBE6V', '2024-10-09 03:59:48', '2000-03-03 07:10:54');
INSERT INTO `room` VALUES ('R701', 60, 'INs0WUOqm4', 'tqyDJZI8wM', '2024-03-22 09:51:35', '2022-03-13 23:55:02');
INSERT INTO `room` VALUES ('R801', 78, 'ibBVBRIR2U', 'pm9UCapeCn', '2024-06-24 12:09:41', '2023-06-15 09:38:11');
INSERT INTO `room` VALUES ('R901', 157, 'cgPiRERobL', '5aG39lJnEG', '2024-01-26 21:32:30', '2019-08-15 09:04:33');

-- ----------------------------
-- Table structure for tc
-- ----------------------------
DROP TABLE IF EXISTS `tc`;
CREATE TABLE `tc`  (
  `Tno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Cno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`Tno`, `Cno`) USING BTREE,
  INDEX `Cno`(`Cno`) USING BTREE,
  CONSTRAINT `tc_ibfk_1` FOREIGN KEY (`Tno`) REFERENCES `teacher` (`Tno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tc_ibfk_2` FOREIGN KEY (`Cno`) REFERENCES `course` (`Cno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tc
-- ----------------------------
INSERT INTO `tc` VALUES ('T111', 'C001');
INSERT INTO `tc` VALUES ('T001', 'C002');
INSERT INTO `tc` VALUES ('T002', 'C002');
INSERT INTO `tc` VALUES ('T001', 'C003');
INSERT INTO `tc` VALUES ('T003', 'C004');
INSERT INTO `tc` VALUES ('T004', 'C005');
INSERT INTO `tc` VALUES ('T005', 'C006');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `Tno` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Tname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Tsex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Ttitle` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Flag` int NOT NULL,
  PRIMARY KEY (`Tno`) USING BTREE,
  INDEX `Tno`(`Tno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('T001', '王明娟', '男', 'Professor', 'john_doe', '482C811DA5D5B4BC6D497FFA98491E38', 0);
INSERT INTO `teacher` VALUES ('T002', '李军', '女', 'Assistant Professor', 'jane_smith', '96B33694C4BB7DBD07391E0BE54745FB', 0);
INSERT INTO `teacher` VALUES ('T003', '赵丽华', '女', 'Lecturer', 'alice _johnson', '7D347CF0EE68174A3588F6CBA31B8A67', 0);
INSERT INTO `teacher` VALUES ('T004', '刘宁', '男', 'Associate Professor', 'mike williams', '8223FE8DC0533C6EBBB717E7FDA2833C', 0);
INSERT INTO `teacher` VALUES ('T005', '陈文明', '女', 'Lecturer', 'emily_brown', 'BA2A48359AA99EA89A95036CDFA72785', 0);
INSERT INTO `teacher` VALUES ('T006', '黄小红', '男', 'Faculty Member', 'faculty_member', 'E10ADC3949BA59ABBE56E057F20F883E', 1);
INSERT INTO `teacher` VALUES ('T007', '吴艳', '男', 'text', 'text', '670B14728AD9902AECBA32E22FA4F6BD', 1);
INSERT INTO `teacher` VALUES ('T008', '徐敏 ', '男', 'text1', 'text1', '96E79218965EB72C92A549DD5A330112', 0);
INSERT INTO `teacher` VALUES ('T009', '马涛 ', '男', 'Professior', '111', '96E79218965EB72C92A549DD5A330112', 0);
INSERT INTO `teacher` VALUES ('T010', '林雅琴', '男', 'Professor', 'jkjk', '96E79218965EB72C92A549DD5A330112', 0);
INSERT INTO `teacher` VALUES ('T011', '郑健 ', '男', 'Professor', 'jiashdau', '96E79218965EB72C92A549DD5A330112', 0);
INSERT INTO `teacher` VALUES ('T111', '许灿', '男', '1', '1', 'D41D8CD98F00B204E9800998ECF8427E', 1);

SET FOREIGN_KEY_CHECKS = 1;
