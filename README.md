# 教室管理信息系统

#### 介绍
##### 大二课设，maven+jdbc+mysql8.0 ^-^
教室管理信息系统<br>
- 基本要求<br>
教师可以进行教室的查询操作，教务员登录后可以进行教室的查询、安排等
- 操作<br>
教室信息，包括教室容纳人数、教室空闲时间、教室设备等教师信息，包括教师姓名、教授课程、教师职称、安排上课时间等.教室安排信息，包括何时空闲、空闲的开始时间、结束时间等

#### 软件架构
三层结构，启动类是src/main/java/com/ClassroomManage/ClassroomManageApplication.java


#### 安装教程

1.  先运行sql文件，创建本地数据库
2.  刷新maven
3.  在resorces包中配置数据库的数据，包括库名字，数据库的用户名和密码

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
