package daotext;

import com.ClassroomManage.dao.impl.RoomDaoImpl;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.service.RoomService;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class RoomDao {

    @Test
    public void insertText(){
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime1 = localDate.atStartOfDay();
        Room room = new Room();
        room.setRno("r111");
        room.setRcapacity(111);
        room.setRlocation("111");
        room.setRequipment("111");
        room.setREtime(localDateTime1);
        room.setRStime(localDateTime1);
        new RoomService().addRoom(room,1);
        System.out.println(1);
    }
}
