package daotext;

import com.ClassroomManage.entity.RC;
import com.ClassroomManage.service.RCService;
import org.testng.annotations.Test;

public class RCDao {

    @Test
    public void removeRcTest(){
        String tno  =  "111";
        String cno = "C001";
        String rno = "R101";
        int flag = 1;
        System.out.println(new RCService().removeRC(rno, cno, tno, flag));
    }

    @Test
    public void selectByEnter(){
        RC rc = new RC();
        //rc.setRno("R101");
        rc.setCno("C002");
        //rc.setTno("T002");

        System.out.println(new RCService().getRCByEnter(rc));
    }

    @Test
    public void selectAll(){
        System.out.println(new RCService().getRCList());
    }
}
