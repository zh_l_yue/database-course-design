package utilstest;

import com.ClassroomManage.utils.TimeCheck;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class timechecktest {
    @Test
    public void testchecktime(){
        TimeCheck timeCheck = new TimeCheck();
        LocalDateTime endTime = LocalDateTime.of(2021, 10, 23, 14, 10, 10);
        LocalDateTime beginTime = LocalDateTime.of(2022, 10, 23, 14, 10, 10);
        System.out.println(timeCheck.checkTime(beginTime, endTime));
        System.out.println(timeCheck.checkTime(endTime, beginTime));
    }
}
