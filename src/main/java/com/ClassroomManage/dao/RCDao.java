package com.ClassroomManage.dao;

import com.ClassroomManage.entity.RC;
import com.ClassroomManage.entity.Room;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Vector;

public interface RCDao {

    /**
     * 管理员添加课程的教室安排信息，返回成功与否
     * @param roomId
     * @param begin
     * @param end
     * @return
     */
    boolean insertRC(String roomId, String cno, String tno, LocalDateTime begin, LocalDateTime end) throws SQLException;

    /**
     * 管理员修改课程的教室安排，返回成功与否
     * @param roomId
     * @param rc
     * @return
     */
    boolean upDataRC(String roomId, String cno , String tno,RC rc) throws SQLException;

    /**
     * 根据教室编号查询信息
     * @param roomId
     * @return
     * @throws Exception
     */
    ArrayList<RC> queryListById(String roomId) throws Exception;

    /**
     * 管理员删除教室安排
     * @param rno
     * @param cno
     * @param tno
     * @return
     */
    boolean delectRC(String rno, String cno, String tno) throws SQLException;

    /**
     * 根据输入的数据查询
     * @param rc
     * @return
     */
    ArrayList<RC> queryRcByEnter(RC rc) throws Exception;

    /**
     * 查询所有rc数据
     * @return
     */
    ArrayList<RC> queryRcList() throws Exception;
}
