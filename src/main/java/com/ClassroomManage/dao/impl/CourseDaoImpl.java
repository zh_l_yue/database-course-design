package com.ClassroomManage.dao.impl;

import com.ClassroomManage.dao.CourseDao;
import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.utils.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CourseDaoImpl extends BaseDao implements CourseDao {
    @Override
    public boolean insertCourse(Course course) throws SQLException {
        int rows = update("insert into course(Cno, Cname, Ccredit) values (?,?,?,?)"
                ,course.getCno(), course.getCname(), getClass());
        return rows > 0;
    }

    @Override
    public boolean upDataCourse(String courseId, Course course) throws SQLException {
        int rows =  update("update course set Cno = ? , Cname = ? , Ccredit = ? where Cno = ? "
                , course.getCno(), course.getCname(), course.getCcredit(), courseId);
        return rows > 0;
    }

    @Override
    public boolean deleteCourse(String courseId) throws SQLException {
        int rows =  update("delete from course where Cno = ?", courseId);
        return rows > 0;
    }

    @Override
    public ArrayList<Course> queryList() throws Exception {
        ArrayList<Course> list = query(Course.class, "select * from course order by Cno");
        return list;
    }

    @Override
    public List<Course> queryLikeListByName(String cname) throws Exception {
        String cnameWithLike = "%" + cname + "%";
        ArrayList<Course> list = query(Course.class, "select * from course where Cname like ? order by Cno asc", cnameWithLike);
        return list;
    }

    @Override
    public List<Course> queryListByTno(String tno) throws Exception {
        ArrayList<Course> list = query(Course.class, "select * from course where Cno in (select Cno from tc where tno = ? ) order by Cno asc", tno);
        return list;
    }

    @Override
    public Course queryByCno(String cno) throws Exception {
        return queryBean(Course.class,"select * from course where Cno = ? ", cno);
    }

    @Override
    public String queryCnoByCname(String cname) throws Exception {
        return queryBean(Course.class,"select * from course where Cname = ? ", cname).getCno();
    }
}
