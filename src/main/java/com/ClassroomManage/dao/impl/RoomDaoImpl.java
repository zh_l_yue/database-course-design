package com.ClassroomManage.dao.impl;

import com.ClassroomManage.dao.RoomDao;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.utils.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoomDaoImpl extends BaseDao implements RoomDao {
    @Override
    public boolean insertRoom(Room room) throws SQLException {
        int rows = update("insert into room(Rno,Rcapacity,Rlocation,Requipment,RStime,REtime) values (?,?,?,?,?,?)"
                , room.getRno(), room.getRcapacity(), room.getRlocation(), room.getRequipment(), room.getRStime(), room.getREtime());

        return rows > 0;
    }

    @Override
    public boolean upDataRoom(String roomId, Room room) throws SQLException {
        int rows =  update("update room set Rno = ? , Rcapacity = ? , Rlocation = ? ,Requipment = ? , RStime = ? , REtime = ? where Rno = ? "
                , room.getRno(), room.getRcapacity(), room.getRlocation(), room.getRequipment(), room.getRStime(), room.getREtime(), roomId);
        return rows > 0;
    }

    @Override
    public boolean delectRoom(String roomId) throws SQLException {
        int rows =  update("delete from room where Rno = ?", roomId);
        return rows > 0;
    }

    @Override
    public ArrayList<Room> queryList() throws Exception {
        ArrayList<Room> list = query(Room.class, "select * from room order by Rno asc");
        return list;
    }

    @Override
    public Room queryById(String roomId) throws Exception {
        Room r  = queryBean(Room.class, "select * from room where Rno = ?",roomId);
        return r;
    }
}
