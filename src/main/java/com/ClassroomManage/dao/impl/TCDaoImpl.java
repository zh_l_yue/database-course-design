package com.ClassroomManage.dao.impl;

import com.ClassroomManage.dao.TCDao;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.entity.TC;
import com.ClassroomManage.utils.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TCDaoImpl extends BaseDao implements TCDao {

    @Override
    public boolean insertTC(TC tc) throws SQLException {
        int rows = update("insert into tc(Tno, Cno) values (?,?)"
                , tc.getTno(), tc.getCno());
        return rows > 0;
    }

    @Override
    public boolean upDateTC(TC tc, String tno, String cno) throws SQLException {
        int rows =  update("update tc set Tno = ? , Cno = ? where Tno = ? and Cno = ?"
                , tc.getTno(), tc.getCno(), tno, cno);
        return rows > 0;
    }

    @Override
    public List<TC> queryTCListByTno(String tno) throws Exception {
        ArrayList<TC> list = query(TC.class, "select * from tc where Tno = ? order by Cno asc", tno);
        return list;
    }

    @Override
    public TC queryTCListByCno(String con) throws Exception {
        TC tc= queryBean(TC.class, "select * from tc where Cno = ? order by Tno asc", con);
        return tc;
    }

    @Override
    public boolean delectTc(String tno, String cno) throws SQLException {
        int rows = 0;
        if(tno == null && cno == null){
            return false;
        }else if(tno == null && cno != null){
            rows =  update("delete from tc where Tno = ? ", tno);
        } else if (cno == null && tno != null) {
            rows =  update("delete from tc where Cno = ? ", cno);
        }else{
            rows =  update("delete from tc where Tno = ? and Cno = ? ", tno, cno);
        }
        return rows > 0;
    }
}
