package com.ClassroomManage.dao.impl;

import com.ClassroomManage.dao.CommonDao;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.utils.BaseDao;
import lombok.extern.slf4j.Slf4j;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;


@Slf4j
public class CommonDaoImpl extends BaseDao implements CommonDao {

    @Override
    public int userLog(String account, String password) throws Exception {
        Teacher teacher = queryBean(Teacher.class,"select * from Teacher where Account = ? and Password = ?",account,password);
        log.info("登录模块sql执行完成");
//        System.out.println(teacher);
        if(teacher==null){
            return -1;//无数据
        }
        //theTeacher = teacher;
        theTeacher.copy(teacher);
        //System.out.println(theTeacher);
        return teacher.getFlag();
    }
}
