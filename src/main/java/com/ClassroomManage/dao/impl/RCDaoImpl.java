package com.ClassroomManage.dao.impl;

import com.ClassroomManage.dao.RCDao;
import com.ClassroomManage.entity.RC;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.utils.BaseDao;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Vector;

public class RCDaoImpl extends BaseDao implements RCDao {
    @Override
    public boolean insertRC(String roomId, String cno, String tno, LocalDateTime begin, LocalDateTime end) throws SQLException {
        int rows = update("insert into rc(Rno,Cno,Tno,UStime,UEtime) values (?,?,?,?,?)"
                , roomId, cno, tno, begin, end);
        return rows > 0;
    }

    @Override
    public boolean upDataRC(String roomId ,String cno ,String tno ,RC rc) throws SQLException {
        int rows =  update("update rc set Rno = ? , Cno = ? ,Tno = ? ,UStime = ? , UEtime = ? where Rno = ? and Cno = ? and Tno = ?"
                ,rc.getRno(), rc.getCno(), rc.getTno(), rc.getUStime(), rc.getUEtime(), roomId, cno, tno);
        return rows > 0;
    }

    @Override
    public ArrayList<RC> queryListById(String roomId) throws Exception {
        ArrayList<RC> list = query(RC.class, "select * from RC where Rno = ? order by UStime asc", roomId);
        return list;
    }

    @Override
    public boolean delectRC(String rno, String cno, String tno) throws SQLException {
        int rows =  update("delete from rc where Rno = ? and Cno = ? and Tno = ? ", rno, cno, tno);
        return rows > 0;
    }

    @Override
    public ArrayList<RC> queryRcByEnter(RC rc) throws Exception {
        Boolean andflag = false;
        StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM rc WHERE ");
        ArrayList<Object> args = new ArrayList<>();
        // 检查RC对象中的每个字段，并动态构建SQL查询
        if (rc.getCno() != null) {
            sqlBuilder.append("Cno = ?");
            args.add(rc.getCno());
            andflag = true;
        }
        if (rc.getTno() != null) {
            sqlBuilder.append(andflag?"and Tno = ?":"Tno = ?");
            args.add(rc.getTno());
            andflag = true;
        }
        if (rc.getRno() != null) {
            sqlBuilder.append(andflag?"and Rno = ?":"Rno = ?");
            args.add(rc.getRno());
            andflag = true;
        }
        if(rc.getUEtime() != null){
            sqlBuilder.append(andflag?"and UEtime = ?":"UEtime = ?");
            args.add(rc.getUEtime());
            andflag = true;
        }
        if (rc.getUStime() != null) {
            sqlBuilder.append(andflag?"and UStime = ?":"UStime = ?");
            args.add(rc.getUStime());
        }
        Object[] argsArray = args.toArray();
        ArrayList<RC> resultList = query(RC.class, sqlBuilder.toString(), argsArray);
        return resultList;
    }

    @Override
    public ArrayList<RC> queryRcList() throws Exception {
        ArrayList<RC> list = query(RC.class, "select * from RC  order by Rno asc, Tno asc, Cno asc");
        return list;
    }


}
