package com.ClassroomManage.dao.impl;

import com.ClassroomManage.dao.TeacherDao;
import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.utils.BaseDao;
import com.ClassroomManage.utils.MD5;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TeacherDaoImpl extends BaseDao implements TeacherDao {
    @Override
    public boolean userAccountCheck(String account) throws Exception {
        Teacher t = queryBean(Teacher.class, "select * from teacher where Account = ?",account);
        return t==null;
    }

    @Override
    public boolean insertTeacher(Teacher teacher) throws SQLException {
        MD5 md5=new MD5();
        String thePassword =  md5.encode(teacher.getPassword());//对密码进行md5加密并且存入数据库
        int rows = update("insert into teacher(Tno, Tname, Tsex, Ttitle, Account ,Password, Flag) values (?,?,?,?,?,?,?)"
                , teacher.getTno(), teacher.getTname(), teacher.getTsex(), teacher.getTtitle(), teacher.getAccount(), thePassword, teacher.getFlag());
        return rows > 0;
    }

    @Override
    public boolean upDataTeacher(String tno, Teacher teacher) throws SQLException {
        MD5 md5=new MD5();
        String thePassword =  md5.encode(teacher.getPassword());//对密码进行md5加密并且存入数据库

        int rows =  update("update teacher set Tno = ? , Tname = ? , Tsex = ? ,Ttitle = ? , Account = ? , Password = ? , Flag = ? where Tno = ? "
                , teacher.getTno(), teacher.getTname(), teacher.getTsex(), teacher.getTtitle(), teacher.getAccount(), thePassword, teacher.getFlag(), tno);
        return rows > 0;
    }

    @Override
    public boolean delectTeacher(String tno, int flag) throws SQLException {
        int rows =  update("delete from teacher where Tno = ? ", tno);
        return rows > 0;
    }

    @Override
    public List<Teacher> queryListTeacher() throws Exception {
        ArrayList<Teacher> list = query(Teacher.class, "select * from teacher order by Tno asc");
        return list;
    }

    @Override
    public Teacher queryListByTno(String tno) throws Exception {
        Teacher t = queryBean(Teacher.class, "select * from teacher where Tno = ?",tno);
        return t;
    }

    @Override
    public List<Teacher> queryLikeListByTname(String tname) throws Exception {
        String tnameWithLike = "%" + tname + "%";
        ArrayList<Teacher> list = query(Teacher.class, "select * from teacher where Tname like ? order by Tno asc", tnameWithLike);
        return list;
    }

    @Override
    public String queryTnoByTname(String tname) throws Exception {
        return queryBean(Teacher.class,"select * from teacher where Tname = ? ", tname).getTno();
    }
}
