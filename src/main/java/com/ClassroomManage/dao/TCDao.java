package com.ClassroomManage.dao;

import com.ClassroomManage.entity.TC;

import java.sql.SQLException;
import java.util.List;

public interface TCDao {

    /**
     * 教务员安排课程
     * @param tc
     * @return
     */
    boolean insertTC(TC tc) throws SQLException;

    /**
     * 教务员修改课程
     * @param tc
     * @return
     */
    boolean upDateTC(TC tc, String tno, String cno) throws SQLException;

    /**
     * 依据教师编号查询课程
     * @param tno
     * @return
     */
    List<TC> queryTCListByTno(String tno) throws Exception;

    /**
     * 依据课程编号查询教师
     * @param con
     * @return
     */
    TC queryTCListByCno(String con) throws Exception;

    /**
     * 删除教师的授课课程信息，传入（tno，null）则删除tno相关，传入（cno，null）则删除cno相关，传入（tno，cno）则精确删除
     * @param tno
     * @param cno
     * @return
     */
    boolean delectTc(String tno, String cno) throws SQLException;
}
