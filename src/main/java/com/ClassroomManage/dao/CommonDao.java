package com.ClassroomManage.dao;

import java.sql.SQLException;

public interface CommonDao {

    /**
     * 用户登录，按照返回的数字确定权限
     * @param account
     * @param password
     * @return
     * @throws Exception
     */
    public int userLog(String account, String password) throws Exception;

}