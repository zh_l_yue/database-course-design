package com.ClassroomManage.dao;

import com.ClassroomManage.entity.Teacher;

import java.sql.SQLException;
import java.util.List;

public interface TeacherDao {

    /**
     * 查看是否为重复账号
     * @param account
     * @return
     */
    boolean userAccountCheck(String account) throws Exception;

    /**
     * 管理员添加教师信息
     * @param teacher
     * @return
     */
    boolean insertTeacher(Teacher teacher) throws SQLException;

    /**
     * 管理员修改教师信息
     * @param tno
     * @param teacher
     * @return
     */
    boolean upDataTeacher(String tno, Teacher teacher) throws SQLException;

    /**
     * 管理员删除教师信息
     * @param tno
     * @param flag
     * @return
     */
    boolean delectTeacher(String tno, int flag) throws SQLException;

    /**
     * 获取所有教师信息
     * @return
     */
    List<Teacher> queryListTeacher() throws Exception;

    /**
     * 获取指定id的教师信息
     * @param tno
     * @return
     */
    Teacher queryListByTno(String tno) throws Exception;

    /**
     * 按照教师名字模糊查询
     * @param tname
     * @return
     */
    List<Teacher> queryLikeListByTname(String tname) throws Exception;

    /**
     * 根据tname返回tno
     * @param tname
     * @return
     */
    String queryTnoByTname(String tname) throws Exception;
}
