package com.ClassroomManage.dao;

import com.ClassroomManage.entity.Room;

import java.sql.SQLException;
import java.util.ArrayList;

public interface RoomDao {

    /**
     * 添加教室信息
     * @param room
     * @return
     */
    boolean insertRoom(Room room) throws SQLException;

    /**
     * 修改教室信息
     * @param roomId
     * @param room
     * @return
     */
    boolean upDataRoom(String roomId, Room room) throws SQLException;

    /**
     * 删除教室信息
     * @param roomId
     * @return
     */
    boolean delectRoom(String roomId) throws SQLException;

    /**
     * 获取所有教室信息
     * @return
     */
    ArrayList<Room> queryList() throws Exception;

    /**
     * 获取所输入编号的教室信息（只有一个）
     * @param roomId
     * @return
     * @throws Exception
     */
    Room queryById(String roomId) throws Exception;
}
