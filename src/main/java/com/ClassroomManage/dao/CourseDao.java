package com.ClassroomManage.dao;

import com.ClassroomManage.entity.Course;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public interface CourseDao {

    /**
     * 插入课程信息
     * @param course
     * @return
     */
    public boolean insertCourse(Course course) throws SQLException;

    /**
     * 修改课程信息
     * @param courseId
     * @param course
     * @return
     */
    boolean upDataCourse(String courseId, Course course) throws SQLException;

    /**
     * 删除课程信息
     * @param courseId
     * @return
     */
    boolean deleteCourse(String courseId) throws SQLException;

    /**
     * 获取所有课程信息
     * @return
     */
    ArrayList<Course> queryList() throws Exception;

    /**
     * 通过名字模糊查询
     * @param cname
     * @return
     */
    List<Course> queryLikeListByName(String cname) throws Exception;

    /**
     * 通过教师编号查教师的授课信息
     * @param tno
     * @return
     * @throws Exception
     */
    List<Course> queryListByTno(String tno) throws Exception;

    /**
     * 通过课程编号找课程信息
     * @param cno
     * @return
     */
    Course queryByCno(String cno) throws Exception;

    /**
     * 通过cname查询cno
     * @param cname
     * @return
     */
    String queryCnoByCname(String cname) throws Exception;
}
