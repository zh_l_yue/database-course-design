package com.ClassroomManage.entity;

import lombok.Data;

/**
 * 教师信息表
 * @date 2024/4/1
 */
@Data
public class Teacher {
    public String Tno;  //教师编号
    public String Tname;  //教师姓名
    public String Tsex;  //教师性别
    public String Ttitle;  //教师职称
    public String Account;  //账号
    public String Password;  //密码
    public int Flag;  //权限（0为教师，1为教务员，-1为失败）

    public void copy(Teacher teacher){
        this.setFlag(teacher.getFlag());
        this.setTno(teacher.getTno());
        this.setTname(teacher.getTname());
        this.setTsex(teacher.getTsex());
        this.setTtitle(teacher.getTtitle());
        this.setAccount(teacher.getAccount());
        //this.setPassword(teacher.getPassword());
    }
}
