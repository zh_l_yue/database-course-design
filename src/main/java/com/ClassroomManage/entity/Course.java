package com.ClassroomManage.entity;

import lombok.Data;

/**
 * 课程信息表
 * @date 2024/4/1
 */
@Data
public class Course {
    public String Cno;  //课程编号
    public String Cname;  //课程名称
    public int Ccredit;  //课程学分
}
