package com.ClassroomManage.entity;

import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 教室信息表
 * @date 2024/4/1
 */
@Data
public class Room {
    public String Rno;  //教室编号
    public int Rcapacity;  //教室可容纳人数
    public String Rlocation;  //教室位置
    public String Requipment;  //配备设备

    public LocalDateTime RStime;  //空闲开始时间
    public LocalDateTime REtime;  //空闲结束时间


}
