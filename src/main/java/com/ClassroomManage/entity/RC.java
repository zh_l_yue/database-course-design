package com.ClassroomManage.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 教室安排信息表
 * @date 2024/4/1
 */
@Data
public class RC {
    public String Rno;  //教室编号
    public String Cno;  //课程编号
    public String Tno;  //教师编号
    public LocalDateTime UStime;  //使用开始时间
    public LocalDateTime UEtime;  //使用结束时间
}
