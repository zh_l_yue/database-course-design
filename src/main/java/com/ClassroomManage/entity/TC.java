package com.ClassroomManage.entity;

import lombok.Data;

/**
 * 授课信息表
 * @date 2024/4/1
 */
@Data
public class TC {
    public String Tno;  //教师编号
    public String Cno;  //课程编号
}
