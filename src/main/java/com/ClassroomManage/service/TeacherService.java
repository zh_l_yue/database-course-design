package com.ClassroomManage.service;

import com.ClassroomManage.dao.TeacherDao;
import com.ClassroomManage.dao.impl.TeacherDaoImpl;
import com.ClassroomManage.entity.Teacher;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

/**
 * 教务员对教师进行curd
 * @author zly
 * @date 2024/4/2
 */
@Slf4j
public class TeacherService {

    public TeacherDao teacherDao = new TeacherDaoImpl();
    /**
     * 增加教师信息，返回是否成功
     * @param teacher
     * @param flag
     * @return
     */
    public boolean addTeacher(Teacher teacher, int flag){
        try {
            return teacherDao.insertTeacher(teacher);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 根据教师编号，修改教师信息，返回是否成功
     * @param Tno
     * @param teacher
     * @param flag
     * @return
     */
    public boolean modifyTeacher(String Tno, Teacher teacher, int flag){
        try {
            return teacherDao.upDataTeacher(Tno, teacher);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 根据教师编号，删除教师信息，返回是否成功
     * @param Tno
     * @param flag
     * @return
     */
    public boolean removeTeacher(String Tno, int flag){
        try {
            return teacherDao.delectTeacher(Tno, flag);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 返回所有教师信息
     * @param
     * @return
     */
    public Vector<Teacher> getTeacherList(){
        try {
            Vector<Teacher> teacherVector = new Vector<Teacher>(teacherDao.queryListTeacher());
            return teacherVector;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 返回指定的教师id的教师
     * @param tno
     * @return
     */
    public Teacher getTeacherListByTno(String tno){
        try {
            Teacher teacher = teacherDao.queryListByTno(tno);
            return teacher;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 按照教师名字模糊查询
     * @param tname
     * @return
     */
    public Vector<Teacher> getLikeTeacherListByTname(String tname){
        try {
            Vector<Teacher> teacherVector = new Vector<Teacher>(teacherDao.queryLikeListByTname(tname));
            return teacherVector;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 根据Tname返回tno
     * @param tname
     * @return
     */
    public String getTnoByTname(String tname){
        try {
            return teacherDao.queryTnoByTname(tname);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 查看是否注册了重复的账号,是返回flase
     * @param account
     * @return
     */
    public boolean checkAccount(String account){


        try {
            return teacherDao.userAccountCheck(account);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return false;
    }

}
