package com.ClassroomManage.service;

import com.ClassroomManage.dao.CommonDao;
import com.ClassroomManage.dao.impl.CommonDaoImpl;
import com.ClassroomManage.utils.MD5;
import lombok.extern.slf4j.Slf4j;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

@Slf4j
public class CommonService {

    private CommonDao commonDao = new CommonDaoImpl();
    /**
     * 根据账号密码登录
     * @param account
     * @param password
     * @return
     */
    public boolean logging(String account, String password){
        log.info("登录模块运行中...");
//        String textAccount = "text";
//        String textPassword = "000000";
        //密码加密
        MD5 md5=new MD5();
        try {
//            commonDao.userLog(account,password);
            commonDao.userLog(account,md5.encode(password));
//            theTeacher.setFlag(commonDao.userLog(textAccount, textPassword));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
//        System.out.println(theTeacher.getTname()+"权限为"+theTeacher.getFlag());
        if(theTeacher.getFlag()!=-1){
            return true;
        }else{
            return false;
        }

    }




}
