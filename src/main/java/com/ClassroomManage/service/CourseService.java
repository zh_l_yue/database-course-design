package com.ClassroomManage.service;

import com.ClassroomManage.dao.CourseDao;
import com.ClassroomManage.dao.impl.CourseDaoImpl;
import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.RC;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

@Slf4j
public class CourseService {

    private CourseDao courseDao = new CourseDaoImpl();
    /**
     * 教务员可以添加课程信息
     * @param course
     * @param flag
     * @return
     */
    public boolean addCourse(Course course, int flag){
        if(flag!=1){
            log.info("无权限添加课程信息");
            return false;
        }
        log.info("正在添加课程" + course.getCname() + "的信息...");
        try {
            return courseDao.insertCourse(course);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 教务员可以根据课程id修改课程信息
     * @param courseId
     * @param flag
     * @param course
     * @return
     */
    public boolean modifyCourse(String courseId, Course course, int flag){
        if(flag!=1){
            log.info("无权限修改课程信息");
            return false;
        }
        log.info("正在修改课程" + courseId + "的信息...");
        try {
            return courseDao.upDataCourse(courseId, course);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 教务员可以删除课程
     * @param courseId
     * @param flag
     * @return
     */
    public boolean removeCourse(String courseId,int flag){
        if(flag!=1){
            log.info("无权限删除课程");
            return false;
        }
        log.info("正在删除" + courseId + "课程...");
        try {
            return courseDao.deleteCourse(courseId);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 查询课程信息
     * @return
     */
    public Vector<Course> getCourseList(){
        try {
            return new Vector<Course>(courseDao.queryList());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 通过cno查询课程信息
     * @param cno
     * @return
     */
    public Course getCourseByCno(String cno){
        try {
            return courseDao.queryByCno(cno);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 通过课程名称模糊查询
     * @param cname
     * @return
     */
    public Vector<Course> getLikeCourseListByName(String cname){
        try {
            return new Vector<Course>(courseDao.queryLikeListByName(cname));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 通过教师编号查教师的授课信息
     * @param tno
     * @return
     */
    public Vector<Course> getCourseListByTno(String tno){
        try {
            return new Vector<Course>(courseDao.queryListByTno(tno));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 根据Cname查询cno
     * @param cname
     * @return
     */
    public String getCnoByCname(String cname){
        try {
            return courseDao.queryCnoByCname(cname);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
