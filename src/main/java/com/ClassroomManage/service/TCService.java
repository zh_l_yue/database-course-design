package com.ClassroomManage.service;

import com.ClassroomManage.dao.TCDao;
import com.ClassroomManage.dao.impl.TCDaoImpl;
import com.ClassroomManage.entity.TC;
import com.ClassroomManage.entity.Teacher;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

@Slf4j
public class TCService {

    public TCDao tcDao = new TCDaoImpl();
    /**
     * 教务员根据教师编号和课程编号，安排课程
     * @param tc
     * @param flag
     * @return
     */
    public boolean setTC(TC tc ,int flag){
        if(flag!=1){
            log.info("无权限安排课程");
            return false;
        }
        try {
            return tcDao.insertTC(tc);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 教务员根据师编号和课程编号，修改课程
     * @param tc
     * @param flag
     * @return
     */
    public boolean modifyTC(String tno, String cno ,TC tc ,int flag){
        if(flag!=1){
            log.info("无权限修改课程");
            return false;
        }
        try {
            return tcDao.upDateTC(tc, tno, cno);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 根据教师编号，查询课程
     * @param Tno
     * @return
     */
    public Vector<TC> getTList(String Tno){
        try {
            Vector<TC> tcVector = new Vector<TC>(tcDao.queryTCListByTno(Tno));
            return tcVector;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 根据课程编号，查询教师
     * @param Con
     * @return
     */
    public TC getCList(String Con){
        try {
            return tcDao.queryTCListByCno(Con);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 根据Tno与Cno，删除课程
     * @param Tno
     * @param Con
     * @param flag
     * @return
     */
    public boolean removeTc(String Tno, String Con, int flag){
        if(flag!=1){
            log.info("无权限删除课程");
            return false;
        }
        try {
            return tcDao.delectTc(Tno, Con);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

}
