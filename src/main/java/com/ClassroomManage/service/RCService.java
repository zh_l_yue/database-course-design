package com.ClassroomManage.service;


import com.ClassroomManage.dao.RCDao;
import com.ClassroomManage.dao.impl.RCDaoImpl;
import com.ClassroomManage.entity.RC;
import com.ClassroomManage.entity.Room;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Vector;

@Slf4j
public class RCService {

    private RCDao rcDao = new RCDaoImpl();

    /**
     * 根据教室编号，与开始结束时间，安排教室使用，返回是否成功
     * @param roomId
     * @param begin
     * @param end
     * @param flag
     * @return
     */
    public boolean setRC(String roomId, String cno, String tno, LocalDateTime begin, LocalDateTime end, int flag){
        if(flag!=1){
            log.info("无权限安排教室...");
            return false;
        }
        log.info("正在安排教室" + roomId + begin + "-" + end + "...");
        try {
            return rcDao.insertRC(roomId, cno, tno, begin, end);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 根据教室id，修改教室使用情况，返回是否成功
     * @param roomId
     * @param flag
     * @return
     */
    public boolean modifyRC(String roomId, String cno, String tno, int flag, RC rc){
        if(flag!=1){
            log.info("无权限修改教室...");
            return false;
        }
        log.info("正在修改教室" + roomId);
        try {
            return rcDao.upDataRC(roomId, cno, tno , rc);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 根据教室id查询这个教室的所有安排
     * @param roomId
     * @return
     */
    public Vector<RC> getRCListById(String roomId){
        log.info("正在查询教室" + roomId + "的所有安排");
        Vector<RC> rcVector = null;
        try {
            rcVector = new Vector<RC>(rcDao.queryListById(roomId));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return rcVector;
    }

    /**
     * 删除教室安排
     * @param rno
     * @param cno
     * @param tno
     * @param flag
     * @return
     */
    public boolean removeRC(String rno, String cno, String tno, int flag){
        if(flag!=1){
            log.info("无权限修删除教室使用情况（RC）...");
            return false;
        }
        try {
            return rcDao.delectRC(rno, cno, tno);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 查询教室安排：允许用户根据不同的条件（例如日期、教室、课程等）查询教室的使用情况。
     * @param rc
     * @return
     */
    public Vector<RC> getRCByEnter(RC rc){
        try {
            return new Vector<>(rcDao.queryRcByEnter(rc));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 查询所有教室的安排
     * @return
     */
    public Vector<RC> getRCList(){
        try {
            return new Vector<>(rcDao.queryRcList());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

}
