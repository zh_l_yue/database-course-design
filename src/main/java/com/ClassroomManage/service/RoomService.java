package com.ClassroomManage.service;


import com.ClassroomManage.dao.RoomDao;
import com.ClassroomManage.dao.impl.RoomDaoImpl;
import com.ClassroomManage.entity.Room;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.Vector;

@Slf4j
public class RoomService {

    public RoomDao roomDao = new RoomDaoImpl();
    /**
     * 教务员添加新教室信息,返回是否成功
     * @param room
     * @param flag
     * @return
     */
    public boolean addRoom(Room room, int flag){
        if(flag!=1){
            log.info("无权限添加教室信息");
            return false;
        }
        log.info("正在添加" + room.getRno() + "的教室信息...");
        try {
            return roomDao.insertRoom(room);
        } catch (SQLException e) {
            log.info(e.getMessage());
        }
        return false;
    }

    /**
     * 根据教室编号修改教室信息，返回是否成功
     * @param roomId
     * @param room
     * @param flag
     * @return
     */
    public boolean modifyRoom(String roomId, Room room ,int flag){
        if(flag!=1){
            log.info("无权限修改教室信息");
            return false;
        }
        log.info("正在修改教室" + roomId + "的信息...");
        try {
            return roomDao.upDataRoom(roomId, room);
        } catch (SQLException e) {
            log.info(e.getMessage());
        }
        return false;
    }

    /**
     * 根据教室id删除教室，返回是否成功
     * @param roomId
     * @param flag
     * @return
     */
    public boolean removeRoom(String roomId, int flag){
        if(flag!=1){
            log.info("无权限删除教室信息");
            return false;
        }
        log.info("正在删除教室"+ roomId +"的信息...");
        try {
            return roomDao.delectRoom(roomId);
        } catch (SQLException e) {
            log.info(e.getMessage());
        }
        return false;
    }

    /**
     * 返回所有教室信息
     * @return
     */
    public Vector<Room> getRoomList(){
        log.info("正在查询所有教室信息...");
        try {
            Vector<Room> roomVector = new Vector<Room>(roomDao.queryList());
//            System.out.println(roomVector);
            return roomVector;
        } catch (Exception e) {
            //e.printStackTrace();
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 按照id查询教室信息
     * @param roomId
     * @return
     */
    public Room getRoomById(String roomId){
        log.info("正在查询id为"+roomId+"的教室信息...");
        try {
            Room room = roomDao.queryById(roomId);
//            System.out.println(roomVector);
            return room;
        } catch (Exception e) {
            //e.printStackTrace();
            log.error(e.getMessage());
        }
        return null;
    }
}
