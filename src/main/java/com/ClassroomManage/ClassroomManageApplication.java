package com.ClassroomManage;

import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.view.loginView.LoginView;
import lombok.extern.slf4j.Slf4j;


@Slf4j  //lambok提供，输出日志方便调试
public class ClassroomManageApplication {
    public static Teacher theTeacher = new Teacher();
    public static void main(String[] args) {
        theTeacher.setFlag(-1);//默认值
        log.info("程序启动....");
        new LoginView();
    }
}
