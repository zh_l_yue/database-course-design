package com.ClassroomManage.utils;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * 用于检测开始时间和结束时间是否符合正常规则
 * @author zly
 * @date 2024/4/9
 */
@Slf4j
public class TimeCheck {
    public boolean checkTime(LocalDateTime begin, LocalDateTime end){
//        LocalDateTime nowTime= LocalDateTime.now();
//        if(begin.isBefore(nowTime)){
//            log.error("当前时间应该在开始时间之前...");
//            return false;
//        }
        if (end.isBefore(begin)) {
            log.error("开始时间应该在结束时间时间之前...");
            return false;
        }
        return true;
    }
}
