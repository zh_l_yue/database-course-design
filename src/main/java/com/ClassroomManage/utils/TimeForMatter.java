package com.ClassroomManage.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 将LocalDateTime换为String并且格式化
 * @author zly
 * @date 2024/4/7
 */
public class TimeForMatter {

    /**
     * 将localDateTime格式转换为String类型的格式
     * @param localDateTime
     * @return
     */
    public String localDateTimeToString(LocalDateTime localDateTime){
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateStr = localDateTime.format(fmt);
        return dateStr;
    }

    /**
     * 将localDateTime格式转换为Date类型的格式
     * @param localDateTime
     * @return
     */
    public Date localDateTimeToData(LocalDateTime localDateTime){
        return Date.from(Timestamp.valueOf(localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).toInstant());
    }

}
