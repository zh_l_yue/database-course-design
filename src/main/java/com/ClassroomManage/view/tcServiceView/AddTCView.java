package com.ClassroomManage.view.tcServiceView;

import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.TeacherService;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class AddTCView extends JFrame {
    public void layoutCenter() {
        Spring accountWidth = Spring.width(CnameLabel);
        Spring accountTxtWidth = Spring.width(CnameLabel);
        Spring spaceWidth = Spring.constant(20);
        Spring childWidth = Spring.sum(Spring.sum(accountWidth,accountTxtWidth),spaceWidth);
        int offsetX = childWidth.getValue() / 2;
        //  布局TnameLabel
        springLayout.putConstraint(SpringLayout.NORTH,TnameLabel,20,SpringLayout.NORTH,centerPanel);
        SpringLayout.Constraints accountC = springLayout.getConstraints(TnameLabel);
        springLayout.putConstraint(SpringLayout.WEST,TnameLabel,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC.setY(Spring.constant(50));
        //  布局TnameTxt
        springLayout.putConstraint(SpringLayout.WEST,TnameTxt,20,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameTxt,0,SpringLayout.NORTH,TnameLabel);

        //  布局CnameLabel
        springLayout.putConstraint(SpringLayout.EAST,CnameLabel,0,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,CnameLabel,20,SpringLayout.SOUTH,TnameLabel);
        //  布局CnameTxt
        springLayout.putConstraint(SpringLayout.WEST,CnameTxt,20,SpringLayout.EAST,CnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,CnameTxt,0,SpringLayout.NORTH,CnameLabel);

        //  布局confirmBtn
        springLayout.putConstraint(SpringLayout.EAST,confirmBtn,0,SpringLayout.EAST,CnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,confirmBtn,20,SpringLayout.SOUTH,CnameLabel);

        //  布局returnBtn
        springLayout.putConstraint(SpringLayout.EAST,returnBtn,0,SpringLayout.EAST,CnameTxt);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,0,SpringLayout.NORTH,confirmBtn);
    }
    JLabel titleLabel = new JLabel("增加教师授课安排",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel TnameLabel = new JLabel("教师名称：");
    JLabel TnoTxt = new JLabel();
    JLabel TnameTxt = new JLabel();
    JLabel CnameLabel = new JLabel("课程名称：");
    JComboBox CnameTxt = new JComboBox();
    JButton returnBtn = new JButton("返回");
    JButton confirmBtn = new JButton("确认添加");
    AddTCHandler addTCHandler;
    public AddTCView(String tno){
        super("增加教室安排");
        addTCHandler = new AddTCHandler(this);
        Container contentPane = getContentPane();

        TeacherService teacherService = new TeacherService();
        Teacher teacher = teacherService.getTeacherListByTno(tno);
        String tname = teacher.getTname();
        CourseService courseService = new CourseService();
        Vector<Course> courseList = courseService.getCourseList();
        for (Course course : courseList) {
            CnameTxt.addItem(course.getCname());
        }

        titleLabel.setFont(new Font("楷体",Font.PLAIN,30));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        TnameLabel.setFont(centerFont);
        TnameTxt.setFont(centerFont);
        TnoTxt.setText(tno);
        TnameTxt.setText(tname);  //设置教师名称
        CnameLabel.setFont(centerFont);
        CnameTxt.setPreferredSize(new Dimension(100,30));
        returnBtn.setFont(centerFont);
        confirmBtn.setFont(centerFont);

        centerPanel.add(TnameLabel);
        centerPanel.add(TnameTxt);
        centerPanel.add(CnameLabel);
        centerPanel.add(CnameTxt);
        centerPanel.add(returnBtn);
        centerPanel.add(confirmBtn);

        returnBtn.addActionListener(addTCHandler);  //按钮监听
        confirmBtn.addActionListener(addTCHandler);  //按钮监听
        //添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();
        //窗口设置
        setSize(500,300);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //这个会连同父页面一起关闭
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JLabel getTnoTxt() {
        return TnoTxt;
    }

    public String getCnameTxt() {
        return CnameTxt.getSelectedItem().toString();
    }
}
