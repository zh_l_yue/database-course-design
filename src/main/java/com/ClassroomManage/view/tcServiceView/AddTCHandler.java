package com.ClassroomManage.view.tcServiceView;

import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.TC;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.TCService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class AddTCHandler implements ActionListener {
    private AddTCView addTCView;
    public AddTCHandler(AddTCView addTCView) {
        this.addTCView = addTCView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            addTCView.dispose();
        } else if ("确认添加".equals(text)) {
            int flag = theTeacher.getFlag();
            TCService tcService = new TCService();
            TC tc = new TC();
            String tno = addTCView.getTnoTxt().getText();
            String cname = addTCView.getCnameTxt();
            //String cno = addTCView.getCnoTxt().getText();
            CourseService courseService = new CourseService();
            String cno = courseService.getCnoByCname(cname);
            Course course = courseService.getCourseByCno(cno);
            tc.setCno(cno);
            tc.setTno(tno);
            if (course != null) {
                if (tcService.setTC(tc,flag)) {
                    JOptionPane.showMessageDialog(addTCView,"添加成功！");
                    addTCView.dispose();
                } else {
                    JOptionPane.showMessageDialog(addTCView,"添加失败！");
                }
            } else {
                JOptionPane.showMessageDialog(addTCView,"课程编号不存在！");
            }
        }
    }
}
