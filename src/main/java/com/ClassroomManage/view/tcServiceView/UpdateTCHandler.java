package com.ClassroomManage.view.tcServiceView;

import com.ClassroomManage.entity.TC;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.TCService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class UpdateTCHandler implements ActionListener {
    private UpdateTCView updateTCView;
    public UpdateTCHandler(UpdateTCView updateTCView) {
        this.updateTCView = updateTCView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            updateTCView.dispose();
        } else if ("确认修改".equals(text)) {
            String tno = updateTCView.getTnoTxt().getText();
            CourseService courseService = new CourseService();
            String oldcname = updateTCView.getOldCnameTxt();
            String oldcno = courseService.getCnoByCname(oldcname);
            String cname = updateTCView.getCnameTxt();
            String cno = courseService.getCnoByCname(cname);
            int flag = theTeacher.getFlag();
            TCService tcService = new TCService();
            TC tc = new TC();
            tc.setTno(updateTCView.getTnoTxt().getText());
            tc.setCno(cno);
            if (tcService.modifyTC(tno,oldcno,tc,flag)) {
                JOptionPane.showMessageDialog(updateTCView,"修改成功！");
                updateTCView.dispose();
            } else {
                JOptionPane.showMessageDialog(updateTCView,"修改失败！");
            }
        }
    }
}
