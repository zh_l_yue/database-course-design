package com.ClassroomManage.view.tcServiceView;

import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.TCService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class DelectTCHandler implements ActionListener {
    private DelectTCView delectTCView;
    public DelectTCHandler(DelectTCView delectTCView) {this.delectTCView = delectTCView;}
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            delectTCView.dispose();
        } else if ("确认删除".equals(text)) {
            int flag = theTeacher.getFlag();
            TCService tcService = new TCService();
            String tno = delectTCView.getTnoTxt().getText();
            CourseService courseService = new CourseService();
            String cname = delectTCView.getCnameTxt();
            String cno = courseService.getCnoByCname(cname);
            //String cno = delectTCView.getCnoTxt().getText();
            if (tcService.removeTc(tno,cno,flag)) {
                JOptionPane.showMessageDialog(delectTCView,"删除成功！");
                delectTCView.dispose();
            } else {
                JOptionPane.showMessageDialog(delectTCView,"删除失败！");
            }
        }
    }
}
