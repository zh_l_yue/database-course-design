package com.ClassroomManage.view.tcServiceView;

import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.view.rcServiceView.DelectRCHandler;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class DelectTCView extends JFrame {
    public void layoutCenter(){
        Spring accountWidth = Spring.width(CnameLabel);
        Spring accountTxtWidth = Spring.width(CnameTxt);
        Spring spaceWidth = Spring.constant(20);
        Spring childWidth = Spring.sum(Spring.sum(accountWidth,accountTxtWidth),spaceWidth);
        int offsetX = childWidth.getValue() / 2;
        //  布局TnameLabel
        springLayout.putConstraint(SpringLayout.NORTH,TnameLabel,20,SpringLayout.NORTH,centerPanel);
        SpringLayout.Constraints accountC = springLayout.getConstraints(TnameLabel);
        springLayout.putConstraint(SpringLayout.WEST,TnameLabel,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC.setY(Spring.constant(50));
        //  布局TnameTxt
        springLayout.putConstraint(SpringLayout.WEST,TnameTxt,20,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameTxt,0,SpringLayout.NORTH,TnameLabel);

        //  布局CnameLabel
        springLayout.putConstraint(SpringLayout.EAST,CnameLabel,0,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,CnameLabel,20,SpringLayout.SOUTH,TnameLabel);
        //  布局CnameTxt
        springLayout.putConstraint(SpringLayout.WEST,CnameTxt,20,SpringLayout.EAST,CnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,CnameTxt,0,SpringLayout.NORTH,CnameLabel);

        //  布局confirmBtn
        springLayout.putConstraint(SpringLayout.EAST,confirmBtn,0,SpringLayout.EAST,CnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,confirmBtn,20,SpringLayout.SOUTH,CnameLabel);

        //  布局returnBtn
        springLayout.putConstraint(SpringLayout.EAST,returnBtn,0,SpringLayout.EAST,CnameTxt);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,0,SpringLayout.NORTH,confirmBtn);
    }
    JLabel titleLabel = new JLabel("删除教师授课安排",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel TnameLabel = new JLabel("教师名称：");
    JLabel TnoTxt = new JLabel();
    JLabel TnameTxt = new JLabel();
    JLabel CnameLabel = new JLabel("课程名称：");
    JComboBox CnameTxt = new JComboBox();
    JButton returnBtn = new JButton("返回");
    JButton confirmBtn = new JButton("确认删除");
    DelectTCHandler delectTCHandler;
    public DelectTCView(String tno){
        super("删除教室安排信息");
        delectTCHandler = new DelectTCHandler(this);
        Container contentPane = getContentPane();

        TeacherService teacherService = new TeacherService();
        Teacher teacher = teacherService.getTeacherListByTno(tno);
        String tname = teacher.getTname();
        CourseService courseService = new CourseService();
        Vector<Course> courseList = courseService.getCourseList();
        for (Course course : courseList) {
            CnameTxt.addItem(course.getCname());
        }

        titleLabel.setFont(new Font("楷体",Font.PLAIN,40));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        returnBtn.setFont(centerFont);
        confirmBtn.setFont(centerFont);

        TnameLabel.setFont(centerFont);
        TnameTxt.setFont(centerFont);
        TnoTxt.setText(tno);  //设置教师编号
        TnameTxt.setText(tname);  //设置教师名称

        CnameLabel.setFont(centerFont);
        CnameTxt.setPreferredSize(new Dimension(100,30));
        TnameLabel.setFont(centerFont);
        TnameTxt.setPreferredSize(new Dimension(100,30));

        centerPanel.add(TnameLabel);
        centerPanel.add(TnameTxt);
        centerPanel.add(CnameLabel);
        centerPanel.add(CnameTxt);
        centerPanel.add(returnBtn);
        centerPanel.add(confirmBtn);
        returnBtn.addActionListener(delectTCHandler);
        confirmBtn.addActionListener(delectTCHandler);
        //  添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();
        //  设置窗口尺寸
        setSize(500,350);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JLabel getTnoTxt() {
        return TnoTxt;
    }

    public String getCnameTxt() {
        return CnameTxt.getSelectedItem().toString();
    }
}
