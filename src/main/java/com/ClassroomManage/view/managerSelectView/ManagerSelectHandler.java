package com.ClassroomManage.view.managerSelectView;

import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.view.roomView.RoomView;
import com.ClassroomManage.view.teacherView.TeacherView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class ManagerSelectHandler implements ActionListener {
    private ManagerSelectView managerSelectView;
    public ManagerSelectHandler(ManagerSelectView managerSelectView) {this.managerSelectView = managerSelectView;}
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        int flag = theTeacher.getFlag();
        if("教室管理表".equals(text)) {
            new RoomView(flag);  //教室管理表
            managerSelectView.dispose();  //关闭选择窗口
        } else if ("教师管理表".equals(text)){
            TeacherService teacherService = new TeacherService();
            Vector<Teacher> teacherList = teacherService.getTeacherList();
            new TeacherView(teacherList);  //教师管理表
            managerSelectView.dispose();  //关闭选择窗口
        }
    }
}
