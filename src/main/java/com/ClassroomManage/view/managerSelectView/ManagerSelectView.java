package com.ClassroomManage.view.managerSelectView;

import javax.swing.*;
import java.awt.*;

public class ManagerSelectView extends JFrame {
    public void layoutCenter() {
        Spring btn1Width = Spring.width(roomInformationBtn);
        Spring btn2Width = Spring.width(teacherInformationBtn);
        Spring spaceWidth = Spring.constant(30);
        Spring childWidth = Spring.sum(Spring.sum(btn1Width,btn2Width),spaceWidth);
        int offsetX = childWidth.getValue() / 2;

        SpringLayout.Constraints accountC = springLayout.getConstraints(roomInformationBtn);
        springLayout.putConstraint(SpringLayout.WEST,roomInformationBtn,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC.setY(Spring.constant(50));

        springLayout.putConstraint(SpringLayout.NORTH,teacherInformationBtn,0,SpringLayout.NORTH,roomInformationBtn);
        springLayout.putConstraint(SpringLayout.WEST,teacherInformationBtn,30,SpringLayout.EAST,roomInformationBtn);
    }

    JLabel titleLabel = new JLabel("请选择要查看的表",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JButton roomInformationBtn = new JButton("教室管理表");
    JButton teacherInformationBtn = new JButton("教师管理表");

    ManagerSelectHandler managerSelectHandler;
    public ManagerSelectView(){
        super("教务员选择页面");
        managerSelectHandler = new ManagerSelectHandler(this);
        Container contentPane = getContentPane();
        titleLabel.setFont(new Font("楷体",Font.PLAIN,30));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        roomInformationBtn.setFont(centerFont);
        teacherInformationBtn.setFont(centerFont);

        centerPanel.add(roomInformationBtn);
        centerPanel.add(teacherInformationBtn);
        roomInformationBtn.addActionListener(managerSelectHandler);
        teacherInformationBtn.addActionListener(managerSelectHandler);

        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();

        //窗口设置
        setSize(500,250);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }
//    public static void main(String args[]) {
//        new ManagerSelectView();
//    }
}
