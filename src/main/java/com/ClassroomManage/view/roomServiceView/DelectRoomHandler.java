package com.ClassroomManage.view.roomServiceView;

import com.ClassroomManage.service.RoomService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class DelectRoomHandler implements ActionListener {
    private DelectRoomView delectRoomView;
    public DelectRoomHandler(DelectRoomView delectRoomView) {
        this.delectRoomView = delectRoomView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            delectRoomView.dispose();
        } else if ("确认删除".equals(text)) {
            int flag = theTeacher.getFlag();
            RoomService roomService = new RoomService();
            String rno = delectRoomView.getRnoLabel().getText();
            roomService.removeRoom(rno,flag);
            JOptionPane.showMessageDialog(delectRoomView,"删除成功！");
            delectRoomView.dispose();
        }
    }
}
