package com.ClassroomManage.view.roomServiceView;

import com.ClassroomManage.entity.Room;
import com.ClassroomManage.utils.TimeForMatter;

import javax.swing.*;
import java.awt.*;

public class SearchRoomView extends JFrame {
    public void layoutCenter() {
        //  按空闲时间那行为标准来居中显示
        Spring LabelWidth1 = Spring.width(REtimeLabel);
        Spring LabelWidth2 = Spring.width(REtimeTxt);
        Spring spaceWidth1 = Spring.constant(20);
        Spring childWidth1 = Spring.sum(Spring.sum(LabelWidth1,LabelWidth2),spaceWidth1);
        int offsetX1 = childWidth1.getValue() / 2;
        SpringLayout.Constraints accountC1 = springLayout.getConstraints(RnoLabel);
        springLayout.putConstraint(SpringLayout.WEST,RnoLabel,-offsetX1,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC1.setY(Spring.constant(50));

        //  四个按钮居中显示
        Spring spaceWidth2 = Spring.constant(60);
        Spring childWidth2 = Spring.sum(Spring.sum(Spring.sum(Spring.sum(Spring.width(returnBtn),Spring.width(updateBtn)),Spring.width(deleteBtn)),Spring.width(planBtn)),spaceWidth2);
        int offsetX2 = childWidth2.getValue() / 2;
        springLayout.putConstraint(SpringLayout.WEST,returnBtn,-offsetX2,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,20,SpringLayout.SOUTH,REtimeLabel);

        //  布局RnoLabel
        springLayout.putConstraint(SpringLayout.NORTH,RnoLabel,20,SpringLayout.NORTH,centerPanel);

        //  布局RnoTxt
        springLayout.putConstraint(SpringLayout.WEST,RnoTxt,20,SpringLayout.EAST,RnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RnoTxt,0,SpringLayout.NORTH,RnoLabel);

        //  布局RlocationLabel
        springLayout.putConstraint(SpringLayout.EAST,RlocationLabel,0,SpringLayout.EAST,RnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RlocationLabel,20,SpringLayout.SOUTH,RnoLabel);

        //  布局RlocationTxt
        springLayout.putConstraint(SpringLayout.WEST,RlocationTxt,20,SpringLayout.EAST,RlocationLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RlocationTxt,0,SpringLayout.NORTH,RlocationLabel);

        //  布局RcapaciyLabel
        springLayout.putConstraint(SpringLayout.EAST,RcapaciyLabel,0,SpringLayout.EAST,RlocationLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RcapaciyLabel,20,SpringLayout.SOUTH,RlocationLabel);

        //  布局RcapaciyTxt
        springLayout.putConstraint(SpringLayout.WEST,RcapaciyTxt,20,SpringLayout.EAST,RcapaciyLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RcapaciyTxt,0,SpringLayout.NORTH,RcapaciyLabel);

        //  布局RequipmentLabel
        springLayout.putConstraint(SpringLayout.EAST,RequipmentLabel,0,SpringLayout.EAST,RcapaciyLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RequipmentLabel,20,SpringLayout.SOUTH,RcapaciyLabel);

        //  布局RequipmentTxt
        springLayout.putConstraint(SpringLayout.WEST,RequipmentTxt,20,SpringLayout.EAST,RequipmentLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RequipmentTxt,0,SpringLayout.NORTH,RequipmentLabel);

        //  布局RStimeLabel
        springLayout.putConstraint(SpringLayout.EAST,RStimeLabel,0,SpringLayout.EAST,RequipmentLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RStimeLabel,20,SpringLayout.SOUTH,RequipmentLabel);

        //  布局RStimeTxt
        springLayout.putConstraint(SpringLayout.WEST,RStimeTxt,20,SpringLayout.EAST,RStimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RStimeTxt,0,SpringLayout.NORTH,RStimeLabel);

        //  布局REtimeLabel
        springLayout.putConstraint(SpringLayout.EAST,REtimeLabel,0,SpringLayout.EAST,RStimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,REtimeLabel,20,SpringLayout.SOUTH,RStimeLabel);

        //  布局REtimeTxt
        springLayout.putConstraint(SpringLayout.WEST,REtimeTxt,20,SpringLayout.EAST,REtimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,REtimeTxt,0,SpringLayout.NORTH,REtimeLabel);

        //  布局updateBtn
        springLayout.putConstraint(SpringLayout.WEST,updateBtn,20,SpringLayout.EAST,returnBtn);
        springLayout.putConstraint(SpringLayout.NORTH,updateBtn,0,SpringLayout.NORTH,returnBtn);

        //  布局deleteBtn
        springLayout.putConstraint(SpringLayout.WEST,deleteBtn,20,SpringLayout.EAST,updateBtn);
        springLayout.putConstraint(SpringLayout.NORTH,deleteBtn,0,SpringLayout.NORTH,updateBtn);

        //  布局planBtn
        springLayout.putConstraint(SpringLayout.WEST,planBtn,20,SpringLayout.EAST,deleteBtn);
        springLayout.putConstraint(SpringLayout.NORTH,planBtn,0,SpringLayout.NORTH,deleteBtn);
    }
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel titleLabel = new JLabel("查看教室信息",JLabel.CENTER);
    JLabel RnoLabel = new JLabel("教 室  编 号：");
    JLabel RnoTxt = new JLabel();
    JLabel RlocationLabel = new JLabel("教 室  位 置：");
    JLabel RlocationTxt = new JLabel();
    JLabel RcapaciyLabel = new JLabel("容 纳  人 数：");
    JLabel RcapaciyTxt = new JLabel();
    JLabel RequipmentLabel = new JLabel("配 备  设 备：");
    JLabel RequipmentTxt = new JLabel();
    JLabel RStimeLabel = new JLabel("空闲开始时间：");
    JLabel RStimeTxt = new JLabel();
    JLabel REtimeLabel = new JLabel("空闲结束时间：");
    JLabel REtimeTxt = new JLabel();
    JButton returnBtn = new JButton("返回");
    JButton updateBtn = new JButton("修改");
    JButton deleteBtn = new JButton("删除");
    JButton planBtn = new JButton("查看安排");
    SearchRoomHandler searchRoomHandler;
    public SearchRoomView(Room searchRoom,int flag){
        super("教室信息");
        searchRoomHandler = new SearchRoomHandler(this);
        Container contentPane = getContentPane();
        TimeForMatter timeForMatter = new TimeForMatter();  //时间格式转换

        //如果flag为0（即教师账号），则隐藏除搜索的其他按钮
        if (flag == 0) {
            updateBtn.setVisible(false);
            deleteBtn.setVisible(false);
        }

        //将接收到的Room的数据放入到对应框中
        RnoTxt.setText(searchRoom.getRno());
        RlocationTxt.setText(searchRoom.getRlocation());
        RcapaciyTxt.setText(String.valueOf(searchRoom.getRcapacity()));
        RequipmentTxt.setText(searchRoom.getRequipment());
        RStimeTxt.setText(timeForMatter.localDateTimeToString(searchRoom.getRStime()));
        REtimeTxt.setText(timeForMatter.localDateTimeToString(searchRoom.getREtime()));

        titleLabel.setFont(new Font("楷体",Font.PLAIN,30));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        RnoLabel.setFont(centerFont);
        RnoTxt.setFont(centerFont);
        RlocationLabel.setFont(centerFont);
        RlocationTxt.setFont(centerFont);
        RcapaciyLabel.setFont(centerFont);
        RcapaciyTxt.setFont(centerFont);
        RequipmentLabel.setFont(centerFont);
        RequipmentTxt.setFont(centerFont);
        RStimeLabel.setFont(centerFont);
        RStimeTxt.setFont(centerFont);
        REtimeLabel.setFont(centerFont);
        REtimeTxt.setFont(centerFont);
        returnBtn.setFont(centerFont);
        updateBtn.setFont(centerFont);
        deleteBtn.setFont(centerFont);
        planBtn.setFont(centerFont);

        centerPanel.add(RnoLabel);
        centerPanel.add(RnoTxt);
        centerPanel.add(RlocationLabel);
        centerPanel.add(RlocationTxt);
        centerPanel.add(RcapaciyLabel);
        centerPanel.add(RcapaciyTxt);
        centerPanel.add(RequipmentLabel);
        centerPanel.add(RequipmentTxt);
        centerPanel.add(RStimeLabel);
        centerPanel.add(RStimeTxt);
        centerPanel.add(REtimeLabel);
        centerPanel.add(REtimeTxt);
        centerPanel.add(returnBtn);
        centerPanel.add(updateBtn);
        centerPanel.add(deleteBtn);
        centerPanel.add(planBtn);

        returnBtn.addActionListener(searchRoomHandler);  //返回按钮监听
        updateBtn.addActionListener(searchRoomHandler);  //修改按钮监听
        deleteBtn.addActionListener(searchRoomHandler);  //删除按钮监听
        planBtn.addActionListener(searchRoomHandler);  //查看教室安排按钮监听

        //添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();

        //窗口设置
        setSize(700,450);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //这个会连同父页面一起关闭
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JLabel getRnoTxt() {
        return RnoTxt;
    }
}
