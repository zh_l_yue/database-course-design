package com.ClassroomManage.view.roomServiceView;

import javax.swing.*;
import java.awt.*;

public class DelectRoomView extends JFrame {
    public void layoutCenter(){
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(returnBtn),Spring.width(confirmBtn)),Spring.constant(20));
        int offsetX = childWidth.getValue() / 2;
        SpringLayout.Constraints accountC = springLayout.getConstraints(returnBtn);
        accountC.setY(Spring.constant(50));
        springLayout.putConstraint(SpringLayout.WEST,returnBtn,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,20,SpringLayout.NORTH,centerPanel);
        //布局确认删除按钮
        springLayout.putConstraint(SpringLayout.WEST,confirmBtn,20,SpringLayout.EAST,returnBtn);
        springLayout.putConstraint(SpringLayout.NORTH,confirmBtn,0,SpringLayout.NORTH,returnBtn);
    }
    JLabel RnoLabel = new JLabel();
    JLabel titleLabel = new JLabel("是否删除该教室信息？",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JButton returnBtn = new JButton("返回");
    JButton confirmBtn = new JButton("确认删除");
    DelectRoomHandler delectRoomHandler;
    public DelectRoomView(String rno){
        super("删除教室信息");
        delectRoomHandler = new DelectRoomHandler(this);
        Container contentPane = getContentPane();

        titleLabel.setFont(new Font("楷体",Font.PLAIN,40));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        returnBtn.setFont(centerFont);
        confirmBtn.setFont(centerFont);

        RnoLabel.setText(rno);

        centerPanel.add(returnBtn);
        centerPanel.add(confirmBtn);
        returnBtn.addActionListener(delectRoomHandler);
        confirmBtn.addActionListener(delectRoomHandler);
        //  添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();
        //  设置窗口尺寸
        setSize(500,200);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }
    public JLabel getRnoLabel() {
        return RnoLabel;
    }
}
