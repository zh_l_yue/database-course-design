package com.ClassroomManage.view.roomServiceView;

import com.ClassroomManage.entity.Room;
import com.ClassroomManage.service.RoomService;
import com.ClassroomManage.utils.TimeCheck;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class UpdateRoomHandler implements ActionListener {
    private UpdateRoomView updateRoomView;
    public UpdateRoomHandler(UpdateRoomView updateRoomView) {
        this.updateRoomView = updateRoomView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        Boolean  dataFlag = false;
        if ("返回".equals(text)) {
            updateRoomView.dispose();
        } else if ("确认修改".equals(text)) {
            int flag = theTeacher.getFlag();
            RoomService roomService = new RoomService();
            TimeCheck timeCheck = new TimeCheck();
            Room room = new Room();
            String rno = updateRoomView.getRnoTxt().getText();
            room.setRno(rno);  //获取教室编号
            //room.setRcapacity(Integer.parseInt(updateRoomView.getRcapaciyTxt().getText()));  //获取容纳人数
            String rcapaText = updateRoomView.getRcapaciyTxt().getText();
            if (rcapaText.matches("\\d+")) {
                room.setRcapacity(Integer.parseInt(rcapaText));  //获取容纳人数
            } else {
                // 提示用户输入的容纳人数无效
                dataFlag = true;
            }
            room.setRlocation(updateRoomView.getRlocationTxt().getText());  //获取教室位置
            room.setRequipment(updateRoomView.getRequipmentTxt().getText());  //获取配备设备
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  //解析时间格式
            room.setRStime(LocalDateTime.parse(updateRoomView.getTimeSelect1().getText(), formatter));  //获取空闲开始时间
            room.setREtime(LocalDateTime.parse(updateRoomView.getTimeSelect2().getText(), formatter));  //获取空闲结束时间
            //修改教室信息
            if (timeCheck.checkTime(LocalDateTime.parse(updateRoomView.getTimeSelect1().getText(), formatter),LocalDateTime.parse(updateRoomView.getTimeSelect2().getText(), formatter))) {
                if(dataFlag){
                    JOptionPane.showMessageDialog(updateRoomView,"容纳人数无效！");

                }else if (roomService.modifyRoom(rno,room,flag)) {
                    JOptionPane.showMessageDialog(updateRoomView,"修改成功！");
                    updateRoomView.dispose();
                } else {
                    JOptionPane.showMessageDialog(updateRoomView,"修改失败！");
                }
            } else {
                JOptionPane.showMessageDialog(updateRoomView,"时间设置错误！");
            }
        }
    }
}
