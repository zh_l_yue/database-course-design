package com.ClassroomManage.view.roomServiceView;

import com.ClassroomManage.entity.Room;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.RoomService;
import com.ClassroomManage.utils.TimeCheck;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;
@Slf4j
public class AddRoomHandler implements ActionListener {

    private AddRoomView addRoomView;
    public AddRoomHandler(AddRoomView addRoomView){
        this.addRoomView = addRoomView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        Boolean  dataFlag = false;
        if("返回".equals(text)) {  //返回按钮（关闭添加教师信息页面）
            addRoomView.dispose();
        } else if ("确认添加".equals(text)){
            int flag = theTeacher.getFlag();
            RoomService roomService = new RoomService();
            TimeCheck timeCheck = new TimeCheck();
            Room room = new Room();
            room.setRno(addRoomView.getRnoTxt().getText());  //获取教室编号
            String rcapaText = addRoomView.getRcapaciyTxt().getText();
            if (rcapaText.matches("\\d+")) {
                room.setRcapacity(Integer.parseInt(rcapaText));  //获取容纳人数
            } else {
                // 提示用户输入的容纳人数无效
                dataFlag = true;
            }
            //room.setRcapacity(Integer.parseInt(addRoomView.getRcapaciyTxt().getText()));  //获取容纳人数
            room.setRlocation(addRoomView.getRlocationTxt().getText());  //获取教室位置
            room.setRequipment(addRoomView.getRequipmentTxt().getText());  //获取配备设备
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  //解析时间格式
            room.setRStime(LocalDateTime.parse(addRoomView.getTimeSelect1().getText(), formatter));  //获取空闲开始时间
            room.setREtime(LocalDateTime.parse(addRoomView.getTimeSelect2().getText(), formatter));  //获取空闲结束时间
            //判断输入的教室编号是否已经存在
            Room temp = roomService.getRoomById(addRoomView.getRnoTxt().getText());
            //添加教室信息
            if (temp == null) {  //temp为空表示没有该教室编号记录
                if(dataFlag){
                    JOptionPane.showMessageDialog(addRoomView,"容纳人数无效！");
                }else if (timeCheck.checkTime(LocalDateTime.parse(addRoomView.getTimeSelect1().getText(), formatter),LocalDateTime.parse(addRoomView.getTimeSelect2().getText(), formatter))) {
                    if (roomService.addRoom(room,flag)) {
                        JOptionPane.showMessageDialog(addRoomView,"添加成功！");
                    }else {
                        JOptionPane.showMessageDialog(addRoomView,"添加失败！");
                    }
                    addRoomView.dispose();
                } else {
                    JOptionPane.showMessageDialog(addRoomView,"时间设置错误！");
                }
            } else {  //temp为空表示有该教室编号记录
                JOptionPane.showMessageDialog(addRoomView,"该教室编号已经存在！");
            }
        }
    }
}
