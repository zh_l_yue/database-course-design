package com.ClassroomManage.view.roomServiceView;

import com.ClassroomManage.view.rcView.RCView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class SearchRoomHandler implements ActionListener {
    private SearchRoomView searchRoomView;
    public SearchRoomHandler(SearchRoomView searchRoomView) {
        this.searchRoomView = searchRoomView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        String rno = searchRoomView.getRnoTxt().getText();
        int flag = theTeacher.getFlag();
        if ("返回".equals(text)) {
            searchRoomView.dispose();
        } else if ("修改".equals(text)) {
            new UpdateRoomView(rno);
        } else if ("删除".equals(text)) {
            new DelectRoomView(rno);
        } else if ("查看安排".equals(text)) {
            searchRoomView.dispose();
            new RCView(rno,flag);
        }
    }
}
