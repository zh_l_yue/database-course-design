package com.ClassroomManage.view.roomServiceView;

import com.ClassroomManage.utils.TimeForMatter;
import com.eltima.components.ui.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Locale;

public class AddRoomView extends JFrame {
    public void layoutCenter() {
        Spring accountWidth = Spring.width(RnoLabel);
        Spring accountTxtWidth = Spring.width(RnoTxt);
        Spring spaceWidth = Spring.constant(20);
        Spring childWidth = Spring.sum(Spring.sum(accountWidth,accountTxtWidth),spaceWidth);
        int offsetX = childWidth.getValue() / 2;
        //  布局RnoLabel
        springLayout.putConstraint(SpringLayout.NORTH,RnoLabel,20,SpringLayout.NORTH,centerPanel);
        //  布局RnoTxt
        springLayout.putConstraint(SpringLayout.WEST,RnoTxt,20,SpringLayout.EAST,RnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RnoTxt,0,SpringLayout.NORTH,RnoLabel);

        //  布局RlocationLabel
        springLayout.putConstraint(SpringLayout.EAST,RlocationLabel,0,SpringLayout.EAST,RnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RlocationLabel,20,SpringLayout.SOUTH,RnoLabel);
        //  布局RlocationTxt
        springLayout.putConstraint(SpringLayout.WEST,RlocationTxt,20,SpringLayout.EAST,RlocationLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RlocationTxt,0,SpringLayout.NORTH,RlocationLabel);

        //  布局RcapaciyLabel
        springLayout.putConstraint(SpringLayout.EAST,RcapaciyLabel,0,SpringLayout.EAST,RlocationLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RcapaciyLabel,20,SpringLayout.SOUTH,RlocationLabel);
        //  布局RcapaciyTxt
        springLayout.putConstraint(SpringLayout.WEST,RcapaciyTxt,20,SpringLayout.EAST,RcapaciyLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RcapaciyTxt,0,SpringLayout.NORTH,RcapaciyLabel);

        //  布局RequipmentLabel
        springLayout.putConstraint(SpringLayout.EAST,RequipmentLabel,0,SpringLayout.EAST,RcapaciyLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RequipmentLabel,20,SpringLayout.SOUTH,RcapaciyLabel);

        //  布局RequipmentTxt
        springLayout.putConstraint(SpringLayout.WEST,RequipmentTxt,20,SpringLayout.EAST,RequipmentLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RequipmentTxt,0,SpringLayout.NORTH,RequipmentLabel);

        //  布局RStimeLabel
        springLayout.putConstraint(SpringLayout.EAST,RStimeLabel,0,SpringLayout.EAST,RequipmentLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RStimeLabel,20,SpringLayout.SOUTH,RequipmentLabel);

        //  布局timeSelect1
        springLayout.putConstraint(SpringLayout.WEST,timeSelect1,20,SpringLayout.EAST,RStimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,timeSelect1,0,SpringLayout.NORTH,RStimeLabel);

        //  布局REtimeLabel
        springLayout.putConstraint(SpringLayout.EAST,REtimeLabel,0,SpringLayout.EAST,RStimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,REtimeLabel,20,SpringLayout.SOUTH,RStimeLabel);

        //  布局timeSelect2
        springLayout.putConstraint(SpringLayout.WEST,timeSelect2,20,SpringLayout.EAST,REtimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,timeSelect2,0,SpringLayout.NORTH,REtimeLabel);

        //  布局confirmBtn
        springLayout.putConstraint(SpringLayout.EAST,confirmBtn,0,SpringLayout.EAST,timeSelect2);
        springLayout.putConstraint(SpringLayout.NORTH,confirmBtn,20,SpringLayout.SOUTH,timeSelect2);

        //  布局returnBtn
        springLayout.putConstraint(SpringLayout.WEST,returnBtn,0,SpringLayout.WEST,REtimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,0,SpringLayout.NORTH,confirmBtn);

        SpringLayout.Constraints accountC = springLayout.getConstraints(RnoLabel);
        springLayout.putConstraint(SpringLayout.WEST,RnoLabel,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC.setY(Spring.constant(50));
    }
    JLabel titleLabel = new JLabel("增加教室信息",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);

    JLabel RnoLabel = new JLabel("教 室  编 号：");
    JTextField RnoTxt = new JTextField();
    JLabel RlocationLabel = new JLabel("教 室  位 置：");
    JTextField RlocationTxt = new JTextField();
    JLabel RcapaciyLabel = new JLabel("容 纳  人 数：");
    JTextField RcapaciyTxt = new JTextField();
    JLabel RequipmentLabel = new JLabel("配 备  设 备：");
    JTextField RequipmentTxt = new JTextField();
    JLabel RStimeLabel = new JLabel("空闲开始时间：");
    DatePicker timeSelect1 = getDatePicker();
    JLabel REtimeLabel = new JLabel("空闲结束时间：");
    DatePicker timeSelect2 = getDatePicker();
    JButton returnBtn = new JButton("返回");
    JButton confirmBtn = new JButton("确认添加");

    AddRoomHandler addRoomHandler;
    public AddRoomView(){
        super("增加教室信息");
        addRoomHandler = new AddRoomHandler(this);
        Container contentPane = getContentPane();

        titleLabel.setFont(new Font("楷体",Font.PLAIN,30));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        RnoLabel.setFont(centerFont);
        RnoTxt.setPreferredSize(new Dimension(200,30));
        RlocationLabel.setFont(centerFont);
        RlocationTxt.setPreferredSize(new Dimension(200,30));
        RcapaciyLabel.setFont(centerFont);
        RcapaciyTxt.setPreferredSize(new Dimension(200,30));
        RequipmentLabel.setFont(centerFont);
        RequipmentTxt.setPreferredSize(new Dimension(200,30));
        RStimeLabel.setFont(centerFont);
        REtimeLabel.setFont(centerFont);
        returnBtn.setFont(centerFont);
        confirmBtn.setFont(centerFont);

        centerPanel.add(RnoLabel);
        centerPanel.add(RnoTxt);
        centerPanel.add(RlocationLabel);
        centerPanel.add(RlocationTxt);
        centerPanel.add(RcapaciyLabel);
        centerPanel.add(RcapaciyTxt);
        centerPanel.add(RequipmentLabel);
        centerPanel.add(RequipmentTxt);
        centerPanel.add(RStimeLabel);
        centerPanel.add(timeSelect1);
        centerPanel.add(REtimeLabel);
        centerPanel.add(timeSelect2);
        centerPanel.add(returnBtn);
        centerPanel.add(confirmBtn);

        returnBtn.addActionListener(addRoomHandler);  //按钮监听
        confirmBtn.addActionListener(addRoomHandler);  //按钮监听
        //添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();
        //窗口设置
        setSize(500,500);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //这个会连同父页面一起关闭
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JTextField getRnoTxt() {
        return RnoTxt;
    }

    public JTextField getRlocationTxt() {
        return RlocationTxt;
    }

    public JTextField getRcapaciyTxt() {
        return RcapaciyTxt;
    }

    public JTextField getRequipmentTxt() {
        return RequipmentTxt;
    }

    public DatePicker getTimeSelect1() {
        return timeSelect1;
    }

    public DatePicker getTimeSelect2() {
        return timeSelect2;
    }

//    public static void main(String args[]) {
//        new AddRoomView();
//    }
    public static DatePicker getDatePicker() {
        DatePicker dp;
        //格式
        String DefaultFormat = "yyyy-MM-dd HH:mm:ss";
        //字体
        Font font = new Font("Times New Roman",Font.BOLD,20);
        //设置大小
        Dimension ds = new Dimension(200,30);
        //当前时间
        TimeForMatter timeForMatter = new TimeForMatter();
        dp = new DatePicker(timeForMatter.localDateTimeToData(LocalDateTime.now()),DefaultFormat,font,ds);
        // 设置国家
        dp.setLocale(Locale.CHINA);
        //设置时钟面板可见
        dp.setTimePanleVisible(true);
        return dp;
    }
}
