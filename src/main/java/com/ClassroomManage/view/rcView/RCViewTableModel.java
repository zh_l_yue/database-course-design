package com.ClassroomManage.view.rcView;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

public class RCViewTableModel extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();
    static {
        columns.addElement("教室编号");
        columns.addElement("课程编号");
        columns.addElement("课程名称");
        columns.addElement("授课教师");
        columns.addElement("上课时间");
        columns.addElement("下课时间");
    }
    private RCViewTableModel() {
        super(null,columns);
    }
    private static RCViewTableModel rcViewTableModel = new RCViewTableModel();
    public static RCViewTableModel assembleModel(Vector<Vector<Object>> data){
        rcViewTableModel.setDataVector(data,columns);
        return rcViewTableModel;
    }
    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
