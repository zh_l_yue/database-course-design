package com.ClassroomManage.view.rcView;

import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.RC;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.utils.TimeForMatter;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class RCView extends JFrame {
    JLabel title = new JLabel();
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JButton returnBtn = new JButton("返回");
    JButton addBtn = new JButton("增加");
    JButton updateBtn = new JButton("修改");
    JButton deleteBtn = new JButton("删除");
    JButton refreshBtn = new JButton("刷新");
    RCViewTable rcViewTable = new RCViewTable();
    RCHandler rcHandler;
    public RCView(String rno,int flag){
        super("教室安排信息表");
        rcHandler = new RCHandler(this);
        Container contentPane = getContentPane();
        layoutNorth(contentPane);  //增删改查按钮
        layoutCenter(contentPane,rno);  //数据显示框
        //如果flag为0（即教师账号），则隐藏除返回的其他按钮
        if (flag == 0) {
            addBtn.setVisible(false);
            updateBtn.setVisible(false);
            deleteBtn.setVisible(false);
            refreshBtn.setVisible(false);
        }
        title.setText(rno);

        //页面设置
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        Rectangle rectangle = new Rectangle(screenInsets.left,screenInsets.top,screenSize.width-screenInsets.left-screenInsets.right,screenSize.height-screenInsets.top-screenInsets.bottom);
        setBounds(rectangle);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);
    }
    public void layoutCenter(Container contentPane,String rno) {  //放入数据
        Vector<Vector<Object>> data = loadTable(rno);
        RCViewTableModel rcViewTableModel = RCViewTableModel.assembleModel(data);
        rcViewTable.setModel(rcViewTableModel);
        rcViewTable.randerRule();
        JScrollPane jScrollPane = new JScrollPane(rcViewTable);
        contentPane.add(jScrollPane,BorderLayout.CENTER);
    }
    public void layoutNorth(Container contentPane) {
        northPanel.add(returnBtn);
        returnBtn.addActionListener(rcHandler);  //按钮监听
        northPanel.add(addBtn);
        addBtn.addActionListener(rcHandler);  //按钮监听
        northPanel.add(updateBtn);
        updateBtn.addActionListener(rcHandler);
        northPanel.add(deleteBtn);
        deleteBtn.addActionListener(rcHandler);
        northPanel.add(refreshBtn);
        refreshBtn.addActionListener(rcHandler);
        contentPane.add(northPanel,BorderLayout.NORTH);
    }
    public Vector<Vector<Object>> loadTable(String rno) {  //加载读取全部数据页面
        RCService rcService = new RCService();
        Vector<RC> rcList = rcService.getRCListById(rno);
        CourseService courseService = new CourseService();
        TeacherService teacherService = new TeacherService();

        Vector<Vector<Object>> data = new Vector<>();
        TimeForMatter timeForMatter = new TimeForMatter();  //时间格式转换
        for(RC rc : rcList) {  //遍历按行添加数据
            Vector<Object> rowVector = new Vector<>();
            rowVector.add(rc.getRno());  //教室编号
            rowVector.add(rc.getCno());  //课程编号
            Course course = courseService.getCourseByCno(rc.getCno());
            rowVector.add(course.getCname());  //课程名称
            String tno = rc.getTno();
            Teacher teacher = teacherService.getTeacherListByTno(tno);  //教师名称
            rowVector.add(teacher.getTname());
            rowVector.add(timeForMatter.localDateTimeToString(rc.getUStime()));  //上课时间
            rowVector.add(timeForMatter.localDateTimeToString(rc.getUEtime()));  //下课时间
            data.addElement(rowVector);
        }
        return data;
    }
    @Override
    public String getTitle() {
        return title.getText();
    }
}
