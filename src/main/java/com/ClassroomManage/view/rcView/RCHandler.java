package com.ClassroomManage.view.rcView;

import com.ClassroomManage.view.rcServiceView.AddRCView;
import com.ClassroomManage.view.rcServiceView.DelectRCView;
import com.ClassroomManage.view.rcServiceView.UpdateRCView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class RCHandler implements ActionListener {
    public RCView rcView;
    public RCHandler(RCView rcView) {
        this.rcView = rcView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        String rno = rcView.getTitle();
        int flag = theTeacher.getFlag();
        if ("返回".equals(text)) {
            rcView.dispose();
        } else if ("增加".equals(text)) {
            new AddRCView(rno);
        } else if ("修改".equals(text)) {
            new UpdateRCView(rno);
        } else if ("删除".equals(text)) {
            new DelectRCView(rno);
        } else if ("刷新".equals(text)) {
            rcView.dispose();
            new RCView(rno, flag);
        }
    }
}
