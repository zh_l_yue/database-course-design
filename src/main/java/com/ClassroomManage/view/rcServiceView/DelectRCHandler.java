package com.ClassroomManage.view.rcServiceView;

import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.TeacherService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class DelectRCHandler implements ActionListener {
    private DelectRCView delectRCView;
    public DelectRCHandler(DelectRCView delectRCView) {this.delectRCView = delectRCView;}
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            delectRCView.dispose();
        } else if ("确认删除".equals(text)) {
            int flag = theTeacher.getFlag();
            RCService rcService = new RCService();
            String rno = delectRCView.getRnoTxt().getText();
            CourseService courseService = new CourseService();
            String cname = delectRCView.getCnameTxt();
            String cno = courseService.getCnoByCname(cname);  //根据选择的课程名称返回课程编号
            //String cno = delectRCView.getCnoTxt().getText();
            TeacherService teacherService = new TeacherService();
            String tname = delectRCView.getTnameTxt();
            String tno = teacherService.getTnoByTname(tname);
            //String tno = delectRCView.getTnoTxt().getText();
            if (rcService.removeRC(rno,cno,tno,flag)) {
                JOptionPane.showMessageDialog(delectRCView,"删除成功！");
                delectRCView.dispose();
            } else {
                JOptionPane.showMessageDialog(delectRCView,"删除失败！");
            }
        }
    }
}
