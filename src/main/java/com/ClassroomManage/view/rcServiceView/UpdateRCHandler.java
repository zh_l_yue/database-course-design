package com.ClassroomManage.view.rcServiceView;

import com.ClassroomManage.entity.RC;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.utils.TimeCheck;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class UpdateRCHandler implements ActionListener {
    private UpdateRCView updateRCView;
    public UpdateRCHandler(UpdateRCView updateRCView) {
        this.updateRCView = updateRCView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            updateRCView.dispose();
        } else if ("确认修改".equals(text)) {
            String roomId = updateRCView.getRnoTxt().getText();
            String cname = updateRCView.getOldCnameTxt();  //旧数据
            //String cno = updateRCView.getOldCnoTxt().getText();
            String tname = updateRCView.getOldTnameTxt();  //旧数据
            //String tno = updateRCView.getOldTnoTxt().getText();
            CourseService courseService = new CourseService();
            String oldcno = courseService.getCnoByCname(cname);  //根据选择的课程名称返回课程编号
            String cno = courseService.getCnoByCname(updateRCView.getCnameTxt());  //新数据
            TeacherService teacherService = new TeacherService();
            String oldtno = teacherService.getTnoByTname(tname);  //根据选择的教师名称返回教师编号
            String tno = teacherService.getTnoByTname(updateRCView.getTnameTxt());   //新数据
            int flag = theTeacher.getFlag();
            RCService rcService = new RCService();
            TimeCheck timeCheck = new TimeCheck();
            RC rc = new RC();  //新数据
            rc.setRno(roomId);
            rc.setCno(cno);
            rc.setTno(tno);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  //解析时间格式
            rc.setUStime(LocalDateTime.parse(updateRCView.getTimeSelect1().getText(), formatter));
            rc.setUEtime(LocalDateTime.parse(updateRCView.getTimeSelect2().getText(), formatter));
            if (timeCheck.checkTime(LocalDateTime.parse(updateRCView.getTimeSelect1().getText(), formatter),LocalDateTime.parse(updateRCView.getTimeSelect2().getText(), formatter))) {
                if (rcService.modifyRC(roomId,oldcno,oldtno,flag,rc)) {
                    JOptionPane.showMessageDialog(updateRCView,"修改成功！");
                    updateRCView.dispose();
                } else {
                    JOptionPane.showMessageDialog(updateRCView,"修改失败！");
                }
            } else {
                JOptionPane.showMessageDialog(updateRCView,"时间设置错误！");
            }
        }
    }
}
