package com.ClassroomManage.view.rcServiceView;

import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.utils.TimeForMatter;
import com.eltima.components.ui.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Vector;

public class AddRCView extends JFrame {
    public void layoutCenter() {
        Spring accountWidth = Spring.width(UEtimeLabel);
        Spring accountTxtWidth = Spring.width(timeSelect2);
        Spring spaceWidth = Spring.constant(20);
        Spring childWidth = Spring.sum(Spring.sum(accountWidth,accountTxtWidth),spaceWidth);
        int offsetX = childWidth.getValue() / 2;
        //  布局RnoLabel
        springLayout.putConstraint(SpringLayout.NORTH,RnoLabel,20,SpringLayout.NORTH,centerPanel);
        SpringLayout.Constraints accountC = springLayout.getConstraints(RnoLabel);
        springLayout.putConstraint(SpringLayout.WEST,RnoLabel,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC.setY(Spring.constant(50));
        //  布局RnoTxt
        springLayout.putConstraint(SpringLayout.WEST,RnoTxt,20,SpringLayout.EAST,RnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,RnoTxt,0,SpringLayout.NORTH,RnoLabel);

        //  布局CnameLabel
        springLayout.putConstraint(SpringLayout.EAST,CnameLabel,0,SpringLayout.EAST,RnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,CnameLabel,20,SpringLayout.SOUTH,RnoLabel);
        //  布局CnameTxt
        springLayout.putConstraint(SpringLayout.WEST,CnameTxt,20,SpringLayout.EAST,CnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,CnameTxt,0,SpringLayout.NORTH,CnameLabel);

        //  布局TnameLabel
        springLayout.putConstraint(SpringLayout.EAST,TnameLabel,0,SpringLayout.EAST,CnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameLabel,20,SpringLayout.SOUTH,CnameLabel);
        //  布局TnameTxt
        springLayout.putConstraint(SpringLayout.WEST,TnameTxt,20,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameTxt,0,SpringLayout.NORTH,TnameLabel);

        //  布局UStimeLabel
        springLayout.putConstraint(SpringLayout.EAST,UStimeLabel,0,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,UStimeLabel,20,SpringLayout.SOUTH,TnameLabel);

        //  布局timeSelect1
        springLayout.putConstraint(SpringLayout.WEST,timeSelect1,20,SpringLayout.EAST,UStimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,timeSelect1,0,SpringLayout.NORTH,UStimeLabel);

        //  布局UEtimeLabel
        springLayout.putConstraint(SpringLayout.EAST,UEtimeLabel,0,SpringLayout.EAST,UStimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,UEtimeLabel,20,SpringLayout.SOUTH,UStimeLabel);

        //  布局timeSelect2
        springLayout.putConstraint(SpringLayout.WEST,timeSelect2,20,SpringLayout.EAST,UEtimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,timeSelect2,0,SpringLayout.NORTH,UEtimeLabel);

        //  布局confirmBtn
        springLayout.putConstraint(SpringLayout.EAST,confirmBtn,0,SpringLayout.EAST,timeSelect2);
        springLayout.putConstraint(SpringLayout.NORTH,confirmBtn,20,SpringLayout.SOUTH,timeSelect2);

        //  布局returnBtn
        springLayout.putConstraint(SpringLayout.WEST,returnBtn,0,SpringLayout.WEST,UEtimeLabel);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,0,SpringLayout.NORTH,confirmBtn);
    }
    JLabel titleLabel = new JLabel("增加教室安排",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel RnoLabel = new JLabel("教室编号：");
    JLabel RnoTxt = new JLabel();
    JLabel CnameLabel = new JLabel("课程名称：");
    JComboBox CnameTxt = new JComboBox();
    JLabel TnameLabel = new JLabel("教师名称：");
    JComboBox TnameTxt = new JComboBox();
    JLabel UStimeLabel = new JLabel("上课时间：");
    DatePicker timeSelect1 = getDatePicker();
    JLabel UEtimeLabel = new JLabel("下课时间：");
    DatePicker timeSelect2 = getDatePicker();
    JButton returnBtn = new JButton("返回");
    JButton confirmBtn = new JButton("确认添加");
    AddRCHandler addRCHandler;
    public AddRCView(String rno){
        super("增加教室安排");
        addRCHandler = new AddRCHandler(this);
        Container contentPane = getContentPane();

        TeacherService teacherService = new TeacherService();
        Vector<Teacher> teacherList = teacherService.getTeacherList();
        for (Teacher teacher : teacherList) {
            TnameTxt.addItem(teacher.getTname());
        }
        CourseService courseService = new CourseService();
        Vector<Course> courseList = courseService.getCourseList();
        for (Course course : courseList) {
            CnameTxt.addItem(course.getCname());
        }


        titleLabel.setFont(new Font("楷体",Font.PLAIN,30));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        RnoLabel.setFont(centerFont);
        RnoTxt.setFont(centerFont);
        RnoTxt.setText(rno);  //设置教室编号
        CnameLabel.setFont(centerFont);
        CnameTxt.setPreferredSize(new Dimension(200,30));
        TnameLabel.setFont(centerFont);
        TnameTxt.setPreferredSize(new Dimension(200,30));
        UStimeLabel.setFont(centerFont);
        UEtimeLabel.setFont(centerFont);
        returnBtn.setFont(centerFont);
        confirmBtn.setFont(centerFont);

        centerPanel.add(RnoLabel);
        centerPanel.add(RnoTxt);
        centerPanel.add(CnameLabel);
        centerPanel.add(CnameTxt);
        centerPanel.add(TnameLabel);
        centerPanel.add(TnameTxt);
        centerPanel.add(UStimeLabel);
        centerPanel.add(timeSelect1);
        centerPanel.add(UEtimeLabel);
        centerPanel.add(timeSelect2);
        centerPanel.add(returnBtn);
        centerPanel.add(confirmBtn);

        returnBtn.addActionListener(addRCHandler);  //按钮监听
        confirmBtn.addActionListener(addRCHandler);  //按钮监听
        //添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();
        //窗口设置
        setSize(500,450);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //这个会连同父页面一起关闭
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JLabel getRnoTxt() {
        return RnoTxt;
    }

    public String getCnameTxt() {
        return CnameTxt.getSelectedItem().toString();
    }

    public String getTnameTxt() {
        return TnameTxt.getSelectedItem().toString();
    }

    public DatePicker getTimeSelect1() {
        return timeSelect1;
    }

    public DatePicker getTimeSelect2() {
        return timeSelect2;
    }

    public static DatePicker getDatePicker() {
        DatePicker dp;
        //格式
        String DefaultFormat = "yyyy-MM-dd HH:mm:ss";
        //字体
        Font font = new Font("Times New Roman",Font.BOLD,20);
        //设置大小
        Dimension ds = new Dimension(200,30);
        //当前时间
        TimeForMatter timeForMatter = new TimeForMatter();
        dp = new DatePicker(timeForMatter.localDateTimeToData(LocalDateTime.now()),DefaultFormat,font,ds);
        // 设置国家
        dp.setLocale(Locale.CHINA);
        //设置时钟面板可见
        dp.setTimePanleVisible(true);
        return dp;
    }
}
