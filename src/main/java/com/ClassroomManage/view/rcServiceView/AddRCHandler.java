package com.ClassroomManage.view.rcServiceView;

import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.utils.TimeCheck;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class AddRCHandler implements ActionListener {
    private AddRCView addRCView;
    public AddRCHandler(AddRCView addRCView) {
        this.addRCView = addRCView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            addRCView.dispose();
        } else if ("确认添加".equals(text)) {
            int flag = theTeacher.getFlag();
            RCService rcService = new RCService();
            TimeCheck timeCheck = new TimeCheck();
            String roomId = addRCView.getRnoTxt().getText();
            String cname = addRCView.getCnameTxt();
            String tname = addRCView.getTnameTxt();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime begin = LocalDateTime.parse(addRCView.getTimeSelect1().getText(),formatter);
            LocalDateTime end = LocalDateTime.parse(addRCView.getTimeSelect2().getText(),formatter);

            TeacherService teacherService = new TeacherService();
            String tno = teacherService.getTnoByTname(tname);  //根据选择的教师名称返回教师编号
            CourseService courseService = new CourseService();
            String cno = courseService.getCnoByCname(cname);  //根据选择的课程名称返回课程编号
            if (rcService.setRC(roomId,cno,tno,begin,end,flag)) {
                if (timeCheck.checkTime(begin,end)) {
                    JOptionPane.showMessageDialog(addRCView,"添加成功！");
                    addRCView.dispose();
                } else {
                    JOptionPane.showMessageDialog(addRCView,"时间设置错误！");
                }
            } else {
                JOptionPane.showMessageDialog(addRCView,"添加失败！");
            }
        }
    }
}
