package com.ClassroomManage.view.teacherServiceView;

import com.ClassroomManage.service.TeacherService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class DelectTeacherHandler implements ActionListener {
    private DelectTeacherView delectTeachermView;
    public DelectTeacherHandler(DelectTeacherView delectTeachermView) {
        this.delectTeachermView = delectTeachermView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            delectTeachermView.dispose();
        } else if ("确认删除".equals(text)) {
            int flag = theTeacher.getFlag();
            TeacherService teacherService = new TeacherService();
            String tno = delectTeachermView.getTnoLabel().getText();
            if (teacherService.removeTeacher(tno,flag)) {
                JOptionPane.showMessageDialog(delectTeachermView,"删除成功！");
            }else {
                JOptionPane.showMessageDialog(delectTeachermView,"删除失败！");
            }
            delectTeachermView.dispose();
        }
    }
}
