package com.ClassroomManage.view.teacherServiceView;

import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.TeacherService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class UpdateTeacherHandler implements ActionListener {
    private UpdateTeacherView updateTeacherView;
    public UpdateTeacherHandler(UpdateTeacherView updateTeacherView) {
        this.updateTeacherView = updateTeacherView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            updateTeacherView.dispose();
        } else if ("确认修改".equals(text)) {
            int flag = theTeacher.getFlag();
            TeacherService teacherService = new TeacherService();
            Teacher teacher = new Teacher();
            String tno = updateTeacherView.getTnoTxt().getText();
            teacher.setTno(tno);
            teacher.setTname(updateTeacherView.getTnameTxt().getText());
            teacher.setTsex(updateTeacherView.getTsexTxt());
            teacher.setTtitle(updateTeacherView.getTtitleTxt());
            teacher.setAccount(updateTeacherView.getAccountTxt().getText());
            teacher.setPassword(updateTeacherView.getPasswordTxt().getText());
            teacher.setFlag(Integer.parseInt(updateTeacherView.getFlagTxt()));
            if (teacherService.modifyTeacher(tno,teacher,flag)) {
                JOptionPane.showMessageDialog(updateTeacherView,"修改成功！");
            } else {
                JOptionPane.showMessageDialog(updateTeacherView,"修改失败！");
            }
            updateTeacherView.dispose();
        }
    }
}
