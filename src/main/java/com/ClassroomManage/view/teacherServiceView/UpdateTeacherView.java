package com.ClassroomManage.view.teacherServiceView;

import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.TeacherService;

import javax.swing.*;
import java.awt.*;

public class UpdateTeacherView extends JFrame {
    public void layoutCenter() {
        Spring accountWidth = Spring.width(TnoLabel);
        Spring accountTxtWidth = Spring.width(TnoTxt);
        Spring spaceWidth = Spring.constant(20);
        Spring childWidth = Spring.sum(Spring.sum(accountWidth,accountTxtWidth),spaceWidth);
        int offsetX = childWidth.getValue() / 2;
        //  布局TnoLabel
        springLayout.putConstraint(SpringLayout.NORTH,TnoLabel,20,SpringLayout.NORTH,centerPanel);
        //  布局TnoTxt
        springLayout.putConstraint(SpringLayout.WEST,TnoTxt,20,SpringLayout.EAST,TnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnoTxt,0,SpringLayout.NORTH,TnoLabel);

        //  布局TnameLabel
        springLayout.putConstraint(SpringLayout.EAST,TnameLabel,0,SpringLayout.EAST,TnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameLabel,20,SpringLayout.SOUTH,TnoLabel);
        //  布局TnameTxt
        springLayout.putConstraint(SpringLayout.WEST,TnameTxt,20,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameTxt,0,SpringLayout.NORTH,TnameLabel);

        //  布局TsexLabel
        springLayout.putConstraint(SpringLayout.EAST,TsexLabel,0,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TsexLabel,20,SpringLayout.SOUTH,TnameLabel);
        //  布局TsexTxt
        springLayout.putConstraint(SpringLayout.WEST,TsexTxt,20,SpringLayout.EAST,TsexLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TsexTxt,0,SpringLayout.NORTH,TsexLabel);

        //  布局TtitleLabel
        springLayout.putConstraint(SpringLayout.EAST,TtitleLabel,0,SpringLayout.EAST,TsexLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TtitleLabel,20,SpringLayout.SOUTH,TsexLabel);
        //  布局TtitleTxt
        springLayout.putConstraint(SpringLayout.WEST,TtitleTxt,20,SpringLayout.EAST,TtitleLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TtitleTxt,0,SpringLayout.NORTH,TtitleLabel);

        //  布局AccountLabel
        springLayout.putConstraint(SpringLayout.EAST,AccountLabel,0,SpringLayout.EAST,TtitleLabel);
        springLayout.putConstraint(SpringLayout.NORTH,AccountLabel,20,SpringLayout.SOUTH,TtitleLabel);
        //  布局AccountTxt
        springLayout.putConstraint(SpringLayout.WEST,AccountTxt,20,SpringLayout.EAST,AccountLabel);
        springLayout.putConstraint(SpringLayout.NORTH,AccountTxt,0,SpringLayout.NORTH,AccountLabel);

        //  布局PasswordLabel
        springLayout.putConstraint(SpringLayout.EAST,PasswordLabel,0,SpringLayout.EAST,AccountLabel);
        springLayout.putConstraint(SpringLayout.NORTH,PasswordLabel,20,SpringLayout.SOUTH,AccountLabel);
        //  布局PasswordTxt
        springLayout.putConstraint(SpringLayout.WEST,PasswordTxt,20,SpringLayout.EAST,PasswordLabel);
        springLayout.putConstraint(SpringLayout.NORTH,PasswordTxt,0,SpringLayout.NORTH,PasswordLabel);

        //  布局FlagLabel
        springLayout.putConstraint(SpringLayout.EAST,FlagLabel,0,SpringLayout.EAST,PasswordLabel);
        springLayout.putConstraint(SpringLayout.NORTH,FlagLabel,20,SpringLayout.SOUTH,PasswordLabel);
        //  布局FlagTxt
        springLayout.putConstraint(SpringLayout.WEST,FlagTxt,20,SpringLayout.EAST,FlagLabel);
        springLayout.putConstraint(SpringLayout.NORTH,FlagTxt,0,SpringLayout.NORTH,FlagLabel);

        //  布局confirmBtn
        springLayout.putConstraint(SpringLayout.EAST,confirmBtn,0,SpringLayout.EAST,FlagTxt);
        springLayout.putConstraint(SpringLayout.NORTH,confirmBtn,20,SpringLayout.SOUTH,FlagTxt);
        //  布局returnBtn
        springLayout.putConstraint(SpringLayout.WEST,returnBtn,0,SpringLayout.WEST,FlagLabel);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,0,SpringLayout.NORTH,confirmBtn);

        SpringLayout.Constraints accountC = springLayout.getConstraints(TnoLabel);
        springLayout.putConstraint(SpringLayout.WEST,TnoLabel,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC.setY(Spring.constant(50));
    }
    JLabel titleLabel = new JLabel("修改教师信息",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel TnoLabel = new JLabel("教师编号：");
    JLabel TnoTxt = new JLabel();
    JLabel TnameLabel = new JLabel("教师姓名：");
    JTextField TnameTxt = new JTextField();
    JLabel TsexLabel = new JLabel("教师性别：");
    JComboBox TsexTxt = new JComboBox();
    JLabel TtitleLabel = new JLabel("教师职称：");
    JComboBox TtitleTxt = new JComboBox();
    JLabel AccountLabel = new JLabel("教师账号：");
    JLabel AccountTxt = new JLabel();
    JLabel PasswordLabel = new JLabel("教师密码：");
    JTextField PasswordTxt = new JTextField();
    JLabel FlagLabel = new JLabel("教师权限：");
    JComboBox FlagTxt = new JComboBox();
    JButton returnBtn = new JButton("返回");
    JButton confirmBtn = new JButton("确认修改");
    UpdateTeacherHandler updateTeacherHandler;
    public UpdateTeacherView(String tno){
        super("修改教师信息");
        updateTeacherHandler = new UpdateTeacherHandler(this);
        Container contentPane = getContentPane();

        titleLabel.setFont(new Font("楷体",Font.PLAIN,30));
        Font centerFont = new Font("楷体",Font.PLAIN,20);

        TsexTxt.addItem("男");
        TsexTxt.addItem("女");

        TtitleTxt.addItem("讲师");
        TtitleTxt.addItem("助教");
        TtitleTxt.addItem("副教授");
        TtitleTxt.addItem("教授");
        TtitleTxt.addItem("教务员");

        FlagTxt.addItem("0");
        FlagTxt.addItem("1");

        TeacherService teacherService = new TeacherService();
        Teacher teacher = teacherService.getTeacherListByTno(tno);

        TnoLabel.setFont(centerFont);
        TnoTxt.setPreferredSize(new Dimension(200,30));
        TnoTxt.setText(tno);
        TnameLabel.setFont(centerFont);
        TnameTxt.setPreferredSize(new Dimension(200,30));
        TnameTxt.setText(teacher.getTname());
        TsexLabel.setFont(centerFont);
        TsexTxt.setPreferredSize(new Dimension(200,30));
        String sex = teacher.getTsex();
        TsexTxt.setSelectedItem(sex);
        TtitleLabel.setFont(centerFont);
        TtitleTxt.setPreferredSize(new Dimension(200,30));
        String title = teacher.getTtitle();
        TtitleTxt.setSelectedItem(title);
        AccountLabel.setFont(centerFont);
        AccountTxt.setPreferredSize(new Dimension(200,30));
        AccountTxt.setText(teacher.getAccount());
        PasswordLabel.setFont(centerFont);
        PasswordTxt.setPreferredSize(new Dimension(200,30));
        FlagLabel.setFont(centerFont);
        FlagTxt.setPreferredSize(new Dimension(200,30));
        String flag = String.valueOf(teacher.getFlag());
        FlagTxt.setSelectedItem(flag);
        returnBtn.setFont(centerFont);
        confirmBtn.setFont(centerFont);

        centerPanel.add(TnoLabel);
        centerPanel.add(TnoTxt);
        centerPanel.add(TnameLabel);
        centerPanel.add(TnameTxt);
        centerPanel.add(TsexLabel);
        centerPanel.add(TsexTxt);
        centerPanel.add(TtitleLabel);
        centerPanel.add(TtitleTxt);
        centerPanel.add(AccountLabel);
        centerPanel.add(AccountTxt);
        centerPanel.add(PasswordLabel);
        centerPanel.add(PasswordTxt);
        centerPanel.add(FlagLabel);
        centerPanel.add(FlagTxt);

        centerPanel.add(returnBtn);
        centerPanel.add(confirmBtn);

        returnBtn.addActionListener(updateTeacherHandler);  //按钮监听
        confirmBtn.addActionListener(updateTeacherHandler);  //按钮监听
        //添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();
        //窗口设置
        setSize(500,500);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //这个会连同父页面一起关闭
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JLabel getTnoTxt() {
        return TnoTxt;
    }

    public JTextField getTnameTxt() {
        return TnameTxt;
    }

    public String getTsexTxt() {
        return TsexTxt.getSelectedItem().toString();
    }

    public String getTtitleTxt() {
        return TtitleTxt.getSelectedItem().toString();
    }

    public JLabel getAccountTxt() {
        return AccountTxt;
    }

    public JTextField getPasswordTxt() {
        return PasswordTxt;
    }

    public String getFlagTxt() {
        return FlagTxt.getSelectedItem().toString();
    }
}
