package com.ClassroomManage.view.teacherServiceView;

import com.ClassroomManage.view.tcView.TCView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class SeacherTeacherHandler implements ActionListener {
    private SearchTeacherView searchTeacherView;
    public SeacherTeacherHandler(SearchTeacherView searchTeacherView) {
        this.searchTeacherView = searchTeacherView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        String tno = searchTeacherView.getTnoTxt().getText();
        int flag = theTeacher.getFlag();
        if ("返回".equals(text)) {
            searchTeacherView.dispose();
        } else if ("修改".equals(text)) {
            new UpdateTeacherView(tno);
        } else if ("删除".equals(text)) {
            new DelectTeacherView(tno);
        } else if ("查看安排".equals(text)) {
            new TCView(tno);
        }
    }
}
