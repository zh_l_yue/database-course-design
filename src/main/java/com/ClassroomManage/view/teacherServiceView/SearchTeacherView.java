package com.ClassroomManage.view.teacherServiceView;

import com.ClassroomManage.entity.Teacher;

import javax.swing.*;
import java.awt.*;

public class SearchTeacherView extends JFrame {
    public void layoutCenter() {
        //  按空闲时间那行为标准来居中显示
        Spring LabelWidth1 = Spring.width(TnoLabel);
        Spring LabelWidth2 = Spring.width(TnoTxt);
        Spring spaceWidth1 = Spring.constant(20);
        Spring childWidth1 = Spring.sum(Spring.sum(LabelWidth1,LabelWidth2),spaceWidth1);
        int offsetX1 = childWidth1.getValue() / 2;

        //  四个按钮居中显示
        Spring spaceWidth2 = Spring.constant(60);
        Spring childWidth2 = Spring.sum(Spring.sum(Spring.sum(Spring.sum(Spring.width(returnBtn),Spring.width(updateBtn)),Spring.width(deleteBtn)),Spring.width(planBtn)),spaceWidth2);
        int offsetX2 = childWidth2.getValue() / 2;
        springLayout.putConstraint(SpringLayout.WEST,returnBtn,-offsetX2,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH,returnBtn,20,SpringLayout.SOUTH,FlagLabel);

        //  布局TnoLabel
        SpringLayout.Constraints accountC1 = springLayout.getConstraints(TnoLabel);
        springLayout.putConstraint(SpringLayout.EAST,TnoLabel,-offsetX1,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC1.setY(Spring.constant(50));
        springLayout.putConstraint(SpringLayout.NORTH,TnoLabel,20,SpringLayout.NORTH,centerPanel);

        //  布局TnoTxt
        springLayout.putConstraint(SpringLayout.WEST,TnoTxt,20,SpringLayout.EAST,TnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnoTxt,0,SpringLayout.NORTH,TnoLabel);

        //  布局TnameLabel
        springLayout.putConstraint(SpringLayout.EAST,TnameLabel,0,SpringLayout.EAST,TnoLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameLabel,20,SpringLayout.SOUTH,TnoLabel);

        //  布局TnameTxt
        springLayout.putConstraint(SpringLayout.WEST,TnameTxt,20,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TnameTxt,0,SpringLayout.NORTH,TnameLabel);

        //  布局TsexLabel
        springLayout.putConstraint(SpringLayout.EAST,TsexLabel,0,SpringLayout.EAST,TnameLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TsexLabel,20,SpringLayout.SOUTH,TnameLabel);

        //  布局TsexTxt
        springLayout.putConstraint(SpringLayout.WEST,TsexTxt,20,SpringLayout.EAST,TsexLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TsexTxt,0,SpringLayout.NORTH,TsexLabel);

        //  布局TtitleLabel
        springLayout.putConstraint(SpringLayout.EAST,TtitleLabel,0,SpringLayout.EAST,TsexLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TtitleLabel,20,SpringLayout.SOUTH,TsexLabel);

        //  布局TtitleTxt
        springLayout.putConstraint(SpringLayout.WEST,TtitleTxt,20,SpringLayout.EAST,TtitleLabel);
        springLayout.putConstraint(SpringLayout.NORTH,TtitleTxt,0,SpringLayout.NORTH,TtitleLabel);

        //  布局AccountLabel
        springLayout.putConstraint(SpringLayout.EAST,AccountLabel,0,SpringLayout.EAST,TtitleLabel);
        springLayout.putConstraint(SpringLayout.NORTH,AccountLabel,20,SpringLayout.SOUTH,TtitleLabel);

        //  布局AccountTxt
        springLayout.putConstraint(SpringLayout.WEST,AccountTxt,20,SpringLayout.EAST,AccountLabel);
        springLayout.putConstraint(SpringLayout.NORTH,AccountTxt,0,SpringLayout.NORTH,AccountLabel);

//        //  布局PasswordLabel
//        springLayout.putConstraint(SpringLayout.EAST,PasswordLabel,0,SpringLayout.EAST,AccountLabel);
//        springLayout.putConstraint(SpringLayout.NORTH,PasswordLabel,20,SpringLayout.SOUTH,AccountLabel);
//
//        //  布局PasswordTxt
//        springLayout.putConstraint(SpringLayout.WEST,PasswordTxt,20,SpringLayout.EAST,PasswordLabel);
//        springLayout.putConstraint(SpringLayout.NORTH,PasswordTxt,0,SpringLayout.NORTH,PasswordLabel);

        //  布局FlagLabel
        springLayout.putConstraint(SpringLayout.EAST,FlagLabel,0,SpringLayout.EAST,AccountLabel);
        springLayout.putConstraint(SpringLayout.NORTH,FlagLabel,20,SpringLayout.SOUTH,AccountLabel);

        //  布局FlagTxt
        springLayout.putConstraint(SpringLayout.WEST,FlagTxt,20,SpringLayout.EAST,FlagLabel);
        springLayout.putConstraint(SpringLayout.NORTH,FlagTxt,0,SpringLayout.NORTH,FlagLabel);

        //  布局updateBtn
        springLayout.putConstraint(SpringLayout.WEST,updateBtn,20,SpringLayout.EAST,returnBtn);
        springLayout.putConstraint(SpringLayout.NORTH,updateBtn,0,SpringLayout.NORTH,returnBtn);

        //  布局deleteBtn
        springLayout.putConstraint(SpringLayout.WEST,deleteBtn,20,SpringLayout.EAST,updateBtn);
        springLayout.putConstraint(SpringLayout.NORTH,deleteBtn,0,SpringLayout.NORTH,updateBtn);

        //  布局planBtn
        springLayout.putConstraint(SpringLayout.WEST,planBtn,20,SpringLayout.EAST,deleteBtn);
        springLayout.putConstraint(SpringLayout.NORTH,planBtn,0,SpringLayout.NORTH,deleteBtn);
    }
    JLabel titleLabel = new JLabel("查看教师信息",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel TnoLabel = new JLabel("教师编号：");
    JLabel TnoTxt = new JLabel();
    JLabel TnameLabel = new JLabel("教师姓名：");
    JLabel TnameTxt = new JLabel();
    JLabel TsexLabel = new JLabel("教师性别：");
    JLabel TsexTxt = new JLabel();
    JLabel TtitleLabel = new JLabel("教师职称：");
    JLabel TtitleTxt = new JLabel();
    JLabel AccountLabel = new JLabel("教师账号：");
    JLabel AccountTxt = new JLabel();
//    JLabel PasswordLabel = new JLabel("教师密码：");
//    JLabel PasswordTxt = new JLabel();
    JLabel FlagLabel = new JLabel("教师权限：");
    JLabel FlagTxt = new JLabel();
    JButton returnBtn = new JButton("返回");
    JButton updateBtn = new JButton("修改");
    JButton deleteBtn = new JButton("删除");
    JButton planBtn = new JButton("查看安排");
    SeacherTeacherHandler seacherTeacherHandler;
    public SearchTeacherView(Teacher seacherteacher){
        super("教师信息");
        seacherTeacherHandler = new SeacherTeacherHandler(this);
        Container contentPane = getContentPane();

        //将接收到的Teacher的数据放入到对应框中
        TnoTxt.setText(seacherteacher.getTno());
        TnameTxt.setText(seacherteacher.getTname());
        TsexTxt.setText(seacherteacher.getTsex());
        TtitleTxt.setText(seacherteacher.getTtitle());
        AccountTxt.setText(seacherteacher.getAccount());
        FlagTxt.setText(String.valueOf(seacherteacher.getFlag()));

        titleLabel.setFont(new Font("楷体",Font.PLAIN,30));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        TnoLabel.setFont(centerFont);
        TnoTxt.setFont(centerFont);
        TnameLabel.setFont(centerFont);
        TnameTxt.setFont(centerFont);
        TsexLabel.setFont(centerFont);
        TsexTxt.setFont(centerFont);
        TtitleLabel.setFont(centerFont);
        TtitleTxt.setFont(centerFont);
        AccountLabel.setFont(centerFont);
        AccountTxt.setFont(centerFont);
        FlagLabel.setFont(centerFont);
        FlagTxt.setFont(centerFont);
        returnBtn.setFont(centerFont);
        updateBtn.setFont(centerFont);
        deleteBtn.setFont(centerFont);
        planBtn.setFont(centerFont);

        centerPanel.add(TnoLabel);
        centerPanel.add(TnoTxt);
        centerPanel.add(TnameLabel);
        centerPanel.add(TnameTxt);
        centerPanel.add(TsexLabel);
        centerPanel.add(TsexTxt);
        centerPanel.add(TtitleLabel);
        centerPanel.add(TtitleTxt);
        centerPanel.add(AccountLabel);
        centerPanel.add(AccountTxt);
        centerPanel.add(FlagLabel);
        centerPanel.add(FlagTxt);
        centerPanel.add(returnBtn);
        centerPanel.add(updateBtn);
        centerPanel.add(deleteBtn);
        centerPanel.add(planBtn);

        returnBtn.addActionListener(seacherTeacherHandler);  //返回按钮监听
        updateBtn.addActionListener(seacherTeacherHandler);  //修改按钮监听
        deleteBtn.addActionListener(seacherTeacherHandler);  //删除按钮监听
        planBtn.addActionListener(seacherTeacherHandler);  //查看教室安排按钮监听

        //添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();

        //窗口设置
        setSize(700,450);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //这个会连同父页面一起关闭
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JLabel getTnoTxt() {
        return TnoTxt;
    }
}
