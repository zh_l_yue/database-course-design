package com.ClassroomManage.view.teacherServiceView;

import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.TeacherService;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;
@Slf4j
public class AddTeacherHandler implements ActionListener {

    private AddTeacherView addTeacherView;
    public AddTeacherHandler(AddTeacherView addTeacherView){
        this.addTeacherView = addTeacherView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //  获取点击了什么按钮
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if("返回".equals(text)) {  //返回按钮（关闭添加教师信息页面）
            addTeacherView.dispose();
        } else if ("确认添加".equals(text)){
            int flag = theTeacher.getFlag();
            TeacherService teacherService = new TeacherService();
            Teacher teacher = new Teacher();
            teacher.setTno(addTeacherView.getTnoTxt().getText());
            teacher.setTname(addTeacherView.getTnameTxt().getText());
            teacher.setTsex(addTeacherView.getTsexTxt());
            teacher.setTtitle(addTeacherView.getTtitleTxt());
            teacher.setAccount(addTeacherView.getAccountTxt().getText());
            teacher.setPassword(addTeacherView.getPasswordTxt().getText());
            teacher.setFlag(Integer.parseInt(addTeacherView.getFlagTxt()));
            Teacher temp = teacherService.getTeacherListByTno(addTeacherView.getTnoTxt().getText());
            //添加教室信息
            if (temp == null) {  //temp为空表示没有该教师编号记录
                if (teacherService.checkAccount(addTeacherView.getAccountTxt().getText())) {
                    teacherService.addTeacher(teacher,flag);
                    addTeacherView.dispose();
                    JOptionPane.showMessageDialog(addTeacherView,"添加成功！");
                } else {
                    JOptionPane.showMessageDialog(addTeacherView,"账号已存在！");
                }
            } else {  //temp为空表示有该教室编号记录
                System.out.println(temp);
                JOptionPane.showMessageDialog(addTeacherView,"该教师编号已经存在！");
            }
        }
    }
}
