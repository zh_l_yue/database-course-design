package com.ClassroomManage.view.teacherView;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

public class TeacherTableModel extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();
    static {
        columns.addElement("教师编号");
        columns.addElement("教师姓名");
        columns.addElement("性别");
        columns.addElement("职称");
        columns.addElement("账号");
        //columns.addElement("密码");
        columns.addElement("权限");
    }
    private TeacherTableModel() {
        super(null,columns);
    }
    private static TeacherTableModel teacherTableModel = new TeacherTableModel();
    public static TeacherTableModel assembleModel(Vector<Vector<Object>> data){
        teacherTableModel.setDataVector(data,columns);
        return teacherTableModel;
    }
    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
