package com.ClassroomManage.view.teacherView;

import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.view.managerSelectView.ManagerSelectView;
import com.ClassroomManage.view.teacherServiceView.AddTeacherView;
import com.ClassroomManage.view.teacherServiceView.SearchTeacherView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class TeacherHandler implements ActionListener {
    public TeacherView teacherView;
    public TeacherHandler(TeacherView teacherView) {
        this.teacherView = teacherView;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        String searchTxt = teacherView.getSearchTxt().getText();  //获取文本框输入的数据
        TeacherService teacherService = new TeacherService();
        Teacher searchTeacher = teacherService.getTeacherListByTno(searchTxt);
        //Vector<Teacher> teacherList1 = teacherService.getTeacherList();
        Vector<Teacher> teacherList2 = teacherService.getLikeTeacherListByTname(searchTxt);
        int flag = theTeacher.getFlag();
        if ("增加".equals(text)) {
            new AddTeacherView();
        } else if ("查找".equals(text)) {
            if (searchTeacher != null) {
                new SearchTeacherView(searchTeacher);
            } else if (teacherList2.size() != 0) {
                teacherView.dispose();
                new TeacherView(teacherList2);
            } else {
                JOptionPane.showMessageDialog(teacherView,"该教师编号或该教师名字不存在！");
            }
        } else if ("刷新".equals(text)) {
            teacherView.dispose();
            Vector<Teacher> teacherList1 = teacherService.getTeacherList();
            new TeacherView(teacherList1);
        }  else if ("退出".equals(text)) {
            teacherView.dispose();
            new ManagerSelectView();
        }
    }
}
