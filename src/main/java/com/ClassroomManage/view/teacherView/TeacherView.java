package com.ClassroomManage.view.teacherView;

import com.ClassroomManage.entity.Teacher;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Vector;

public class TeacherView extends JFrame {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JButton exitBtn = new JButton("退出");
    JButton addBtn = new JButton("增加");
    JTextField searchTxt = new JTextField(15);
    JButton searchBtn = new JButton("查找");
    JButton refreshBtn = new JButton("刷新");
    TeacherViewTable teacherViewTable = new TeacherViewTable();
    TeacherHandler teacherHandler;
    public TeacherView(Vector<Teacher> teacherList){
        super("教师信息管理表");
        teacherHandler = new TeacherHandler(this);
        Container contentPane = getContentPane();
        layoutNorth(contentPane);  //增删改查按钮
        layoutCenter(contentPane,teacherList);  //数据显示框

        //页面设置
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        Rectangle rectangle = new Rectangle(screenInsets.left,screenInsets.top,screenSize.width-screenInsets.left-screenInsets.right,screenSize.height-screenInsets.top-screenInsets.bottom);
        setBounds(rectangle);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);
    }
    public void layoutCenter(Container contentPane,Vector<Teacher> teacherList) {  //放入数据
        Vector<Vector<Object>> data = loadTable(teacherList);
        TeacherTableModel teacherTableModel = TeacherTableModel.assembleModel(data);
        teacherViewTable.setModel(teacherTableModel);
        teacherViewTable.randerRule();
        JScrollPane jScrollPane = new JScrollPane(teacherViewTable);
        contentPane.add(jScrollPane,BorderLayout.CENTER);
    }
    public void layoutNorth(Container contentPane) {
        northPanel.add(exitBtn);
        exitBtn.addActionListener(teacherHandler);
        northPanel.add(addBtn);
        addBtn.addActionListener(teacherHandler);  //按钮监听
        northPanel.add(searchTxt);
        northPanel.add(searchBtn);
        searchBtn.addActionListener(teacherHandler);
        northPanel.add(refreshBtn);
        refreshBtn.addActionListener(teacherHandler);

        contentPane.add(northPanel,BorderLayout.NORTH);
    }
    public Vector<Vector<Object>> loadTable(Vector<Teacher> teacherList) {  //加载读取全部数据页面
        Vector<Vector<Object>> data = new Vector<>();
        for(Teacher teacher : teacherList) {  //遍历按行添加数据
            Vector<Object> rowVector = new Vector<>();
            rowVector.add(teacher.getTno());
            rowVector.add(teacher.getTname());
            rowVector.add(teacher.getTsex());
            rowVector.add(teacher.getTtitle());
            rowVector.add(teacher.getAccount());
            //rowVector.add(teacher.getPassword());
            rowVector.add(teacher.getFlag());
            data.addElement(rowVector);
        }
        return data;
    }

    public JTextField getSearchTxt() {
        return searchTxt;
    }
}
