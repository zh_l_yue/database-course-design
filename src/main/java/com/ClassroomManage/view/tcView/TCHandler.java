package com.ClassroomManage.view.tcView;

import com.ClassroomManage.view.tcServiceView.AddTCView;
import com.ClassroomManage.view.tcServiceView.DelectTCView;
import com.ClassroomManage.view.tcServiceView.UpdateTCView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class TCHandler implements ActionListener {
    public TCView tcView;
    public TCHandler(TCView tcView) {
        this.tcView = tcView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        String tno = tcView.getTitle();
        int flag = theTeacher.getFlag();
        if ("返回".equals(text)) {
            tcView.dispose();
        } else if ("增加".equals(text)) {
            new AddTCView(tno);
        } else if ("修改".equals(text)) {
            new UpdateTCView(tno);
        } else if ("删除".equals(text)) {
            new DelectTCView(tno);
        } else if ("刷新".equals(text)) {
            tcView.dispose();
            new TCView(tno);
        }
    }
}
