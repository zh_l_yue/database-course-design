package com.ClassroomManage.view.tcView;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

public class TCViewTableModel extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();
    static {
        columns.addElement("教师编号");
        columns.addElement("教师姓名");
        columns.addElement("课程编号");
        columns.addElement("课程名称");
    }
    private TCViewTableModel() {
        super(null,columns);
    }
    private static TCViewTableModel tcViewTableModel = new TCViewTableModel();
    public static TCViewTableModel assembleModel(Vector<Vector<Object>> data) {
        tcViewTableModel.setDataVector(data,columns);
        return tcViewTableModel;
    }
    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
