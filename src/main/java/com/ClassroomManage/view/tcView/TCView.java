package com.ClassroomManage.view.tcView;

import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.TC;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.TCService;
import com.ClassroomManage.service.TeacherService;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class TCView extends JFrame {
    JLabel title = new JLabel();
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JButton returnBtn = new JButton("返回");
    JButton addBtn = new JButton("增加");
    JButton updateBtn = new JButton("修改");
    JButton deleteBtn = new JButton("删除");
    JButton refreshBtn = new JButton("刷新");
    TCViewTable tcViewTable = new TCViewTable();
    TCHandler tcHandler;
    public TCView(String tno){
        super("教师授课信息");
        tcHandler = new TCHandler(this);
        Container contentPane = getContentPane();
        layoutNorth(contentPane);  //增删改查按钮
        layoutCenter(contentPane,tno);  //数据显示框

        title.setText(tno);

        //页面设置
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        Rectangle rectangle = new Rectangle(screenInsets.left,screenInsets.top,screenSize.width-screenInsets.left-screenInsets.right,screenSize.height-screenInsets.top-screenInsets.bottom);
        setBounds(rectangle);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);
    }

    public void layoutCenter(Container contentPane,String tno) {  //放入数据
        Vector<Vector<Object>> data = loadTable(tno);
        TCViewTableModel tcViewTableModel = TCViewTableModel.assembleModel(data);
        tcViewTable.setModel(tcViewTableModel);
        tcViewTable.randerRule();
        JScrollPane jScrollPane = new JScrollPane(tcViewTable);
        contentPane.add(jScrollPane,BorderLayout.CENTER);
    }
    public void layoutNorth(Container contentPane) {
        northPanel.add(returnBtn);
        returnBtn.addActionListener(tcHandler);  //按钮监听
        northPanel.add(addBtn);
        addBtn.addActionListener(tcHandler);  //按钮监听
        northPanel.add(updateBtn);
        updateBtn.addActionListener(tcHandler);
        northPanel.add(deleteBtn);
        deleteBtn.addActionListener(tcHandler);
        northPanel.add(refreshBtn);
        refreshBtn.addActionListener(tcHandler);
        contentPane.add(northPanel,BorderLayout.NORTH);
    }
    public Vector<Vector<Object>> loadTable(String tno) {  //加载读取全部数据页面
        TCService tcService = new TCService();
        Vector<TC> tcList = tcService.getTList(tno);
        TeacherService teacherService = new TeacherService();
        Teacher teacher = teacherService.getTeacherListByTno(tno);
        String tname = teacher.getTname();
        CourseService courseService = new CourseService();

        Vector<Vector<Object>> data = new Vector<>();
        for(TC tc : tcList) {  //遍历按行添加数据
            Vector<Object> rowVector = new Vector<>();
            rowVector.add(tno);  //教师编号
            rowVector.add(tname);  //教师名称
            rowVector.add(tc.getCno());  //课程编号
            Course course = courseService.getCourseByCno(tc.getCno());
            rowVector.add(course.getCname());  //课程名称
            data.addElement(rowVector);
        }
        return data;
    }

    @Override
    public String getTitle() {
        return title.getText();
    }
}
