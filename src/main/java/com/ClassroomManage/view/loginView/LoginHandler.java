package com.ClassroomManage.view.loginView;

import com.ClassroomManage.service.CommonService;
import com.ClassroomManage.view.managerSelectView.ManagerSelectView;
import com.ClassroomManage.view.roomView.RoomView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;


public class LoginHandler extends KeyAdapter implements ActionListener {

    private CommonService commonService = new CommonService();
    private LoginView loginView;
    public LoginHandler(LoginView loginView){
        this.loginView = loginView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if("重置".equals(text)) {  //重置账号密码框
            loginView.getAccountTxt().setText("");
            loginView.getPwdField().setText("");
        } else if ("登录".equals(text)){
            String account = loginView.getAccountTxt().getText();  //获取账号
            String pwd = new String(loginView.getPwdField().getPassword());  //获取密码（暗文）
            boolean flag0 = commonService.logging(account,pwd);  //判断账号密码是否正确
            if (!flag0) {
                JOptionPane.showMessageDialog(loginView,"账号或者密码错误!");
            } else {
                JOptionPane.showMessageDialog(loginView,"账号密码正确!");
                loginView.dispose();  //关闭登录页面
                int flag = theTeacher.getFlag();
                if (flag == 1) {
                    new ManagerSelectView();
                } else if (flag == 0) {
                    new RoomView(flag);
                }
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {  //与上面一样
        if (KeyEvent.VK_ENTER == e.getKeyCode()) {
            String account = loginView.getAccountTxt().getText();  //获取账号
            String pwd = new String(loginView.getPwdField().getPassword());  //获取密码（暗文）
            boolean flag0 = commonService.logging(account,pwd);  //判断账号密码是否正确
            if (!flag0) {
                JOptionPane.showMessageDialog(loginView,"账号或者密码错误!");
            } else {
                JOptionPane.showMessageDialog(loginView,"账号密码正确!");
                loginView.dispose();  //关闭登录页面
                int flag = theTeacher.getFlag();
                if (flag == 1) {  //1为教务员
                    new ManagerSelectView();
                } else if (flag == 0) {  //0为教师
                    new RoomView(flag);
                }
            }
        }
    }
}
