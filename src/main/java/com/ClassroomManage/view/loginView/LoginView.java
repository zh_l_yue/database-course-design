package com.ClassroomManage.view.loginView;

import javax.swing.*;
import java.awt.*;

public class LoginView extends JFrame {

    public void layoutCenter(){
        //  布局accountLabel
        Spring accountWidth = Spring.width(accountLabel);
        Spring accountTxtWidth = Spring.width(accountTxt);
        Spring spaceWidth = Spring.constant(20);
        Spring childWidth = Spring.sum(Spring.sum(accountWidth,accountTxtWidth),spaceWidth);
        int offsetX = childWidth.getValue() / 2;
        springLayout.putConstraint(SpringLayout.NORTH,accountLabel,20,SpringLayout.NORTH,centerPanel);

        //  布局accountTxt
        springLayout.putConstraint(SpringLayout.WEST,accountTxt,20,SpringLayout.EAST,accountLabel);
        springLayout.putConstraint(SpringLayout.NORTH,accountTxt,0,SpringLayout.NORTH,accountLabel);

        //  布局pwdLabel
        springLayout.putConstraint(SpringLayout.EAST,pwdLabel,0,SpringLayout.EAST,accountLabel);
        springLayout.putConstraint(SpringLayout.NORTH,pwdLabel,20,SpringLayout.SOUTH,accountLabel);

        //  布局pwdTxt
        springLayout.putConstraint(SpringLayout.WEST,pwdField,20,SpringLayout.EAST,pwdLabel);
        springLayout.putConstraint(SpringLayout.NORTH,pwdField,0,SpringLayout.NORTH,pwdLabel);

        //  布局loginBtn
        springLayout.putConstraint(SpringLayout.WEST,loginBtn,0,SpringLayout.WEST,pwdLabel);
        springLayout.putConstraint(SpringLayout.NORTH,loginBtn,20,SpringLayout.SOUTH,pwdLabel);

        //  布局resetBtn
        springLayout.putConstraint(SpringLayout.EAST,resetBtn,0,SpringLayout.EAST,pwdField);
        springLayout.putConstraint(SpringLayout.NORTH,resetBtn,20,SpringLayout.SOUTH,pwdField);

        SpringLayout.Constraints accountC = springLayout.getConstraints(accountLabel);
        springLayout.putConstraint(SpringLayout.WEST,accountLabel,-offsetX,SpringLayout.HORIZONTAL_CENTER,centerPanel);
        accountC.setY(Spring.constant(50));
    }
    JLabel titleLabel = new JLabel("教室管理系统",JLabel.CENTER);
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel accountLabel = new JLabel("账号：");
    JTextField accountTxt = new JTextField();
    JLabel pwdLabel = new JLabel("密码：");
    JPasswordField pwdField = new JPasswordField();
    JButton loginBtn = new JButton("登录");
    JButton resetBtn = new JButton("重置");

    LoginHandler loginHandler;
    public LoginView() {
        super("教室管理系统");
        loginHandler = new LoginHandler(this);
        Container contentPane = getContentPane();

        titleLabel.setFont(new Font("楷体",Font.PLAIN,40));
        Font centerFont = new Font("楷体",Font.PLAIN,20);
        accountLabel.setFont(centerFont);
        accountTxt.setPreferredSize(new Dimension(200,30));
        pwdLabel.setFont(centerFont);
        pwdField.setPreferredSize(new Dimension(200,30));
        loginBtn.setFont(centerFont);
        resetBtn.setFont(centerFont);

        centerPanel.add(accountLabel);
        centerPanel.add(accountTxt);
        centerPanel.add(pwdLabel);
        centerPanel.add(pwdField);
        centerPanel.add(loginBtn);
        loginBtn.addActionListener(loginHandler);  //按钮监听
        loginBtn.addKeyListener(loginHandler);  //回车监听
        centerPanel.add(resetBtn);
        resetBtn.addActionListener(loginHandler);  //按钮监听
        //  添加组件
        contentPane.add(titleLabel,BorderLayout.NORTH);
        contentPane.add(centerPanel,BorderLayout.CENTER);
        layoutCenter();

        //  设置loginBtn为默认按钮
        getRootPane().setDefaultButton(loginBtn);
        //  设置窗口尺寸
        setSize(500,300);
        setLocationRelativeTo(null);
        //屏幕居中
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  //获取屏幕尺寸
        int x = (int)((screenSize.getWidth())-getWidth()) / 2;
        int y = (int)((screenSize.getHeight())-getHeight()) / 2;
        setLocation(x,y);
        //关闭窗口
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        //窗口可见
        setResizable(true);
    }

    public JTextField getAccountTxt() {
        return accountTxt;
    }

    public JPasswordField getPwdField() {
        return pwdField;
    }

//    public static void main(String args[]) {
//        new LoginView();
//    }
}
