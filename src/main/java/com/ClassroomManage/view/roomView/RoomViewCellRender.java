package com.ClassroomManage.view.roomView;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class RoomViewCellRender extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if(row % 2 == 0) {
            setBackground(Color.LIGHT_GRAY);  //隔行换色
        }else {
            setBackground(Color.WHITE);
        }
        setHorizontalAlignment(DefaultTableCellRenderer.CENTER);  //文字居中
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}