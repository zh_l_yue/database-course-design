package com.ClassroomManage.view.roomView;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.Vector;

public class RoomViewTable extends JTable {
    public RoomViewTable(){
        //设置表头
        JTableHeader tableHeader = getTableHeader();
        tableHeader.setFont(new Font(null,Font.BOLD,16));
        //设置表格
        setFont(new Font(null,Font.PLAIN,14));
        setRowHeight(30);
    }
    public void randerRule(){
        //设置列的渲染(居中)
        Vector<String> columns = RoomViewTableModel.getColumns();
        RoomViewCellRender render = new RoomViewCellRender();
        for (int i = 0;i < columns.size();i++){
            TableColumn column = getColumn(columns.get(i));
            column.setCellRenderer(render);
        }
    }
}