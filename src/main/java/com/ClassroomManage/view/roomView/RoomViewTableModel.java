package com.ClassroomManage.view.roomView;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

public class RoomViewTableModel extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();
    static {  //列名
        columns.addElement("教室编号");
        columns.addElement("教室位置");
        columns.addElement("容纳人数");
        columns.addElement("配备设备");
        columns.addElement("空闲开始时间");
        columns.addElement("空闲结束时间");
    }
    private RoomViewTableModel(){
        super(null,columns);
    }
    private static RoomViewTableModel roomViewTableModel = new RoomViewTableModel();
    public static RoomViewTableModel assembleModel(Vector<Vector<Object>> data){
        roomViewTableModel.setDataVector(data,columns);
        return roomViewTableModel;
    }
    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
