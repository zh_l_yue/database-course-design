package com.ClassroomManage.view.roomView;

import com.ClassroomManage.entity.Room;
import com.ClassroomManage.service.RoomService;
import com.ClassroomManage.utils.TimeForMatter;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class RoomView extends JFrame {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JButton returnBtn = new JButton("返回");
    JButton exitBtn = new JButton("退出");
    JButton addBtn = new JButton("增加");
    JTextField searchTxt = new JTextField(15);
    JButton searchBtn = new JButton("查找");
    JButton refreshBtn = new JButton("刷新");
    JButton allRCBtn = new JButton("查看所有教室安排信息");
    RoomViewTable mainViewTable = new RoomViewTable();
    RoomHandler roomHandler;
    public RoomView(int flag){
        super("教室信息管理表");
        roomHandler = new RoomHandler(this);
        Container contentPane = getContentPane();
        layoutNorth(contentPane);  //增删改查按钮
        layoutCenter(contentPane);  //数据显示框
        //如果flag为0（即教师账号），则隐藏除搜索的其他按钮
        if (flag == 0) {
            returnBtn.setVisible(false);
            addBtn.setVisible(false);
            refreshBtn.setVisible(false);
        } else {
            exitBtn.setVisible(false);
        }

        //页面设置
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        Rectangle rectangle = new Rectangle(screenInsets.left,screenInsets.top,screenSize.width-screenInsets.left-screenInsets.right,screenSize.height-screenInsets.top-screenInsets.bottom);
        setBounds(rectangle);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);
    }

    public void layoutCenter(Container contentPane) {  //放入数据
        Vector<Vector<Object>> data = loadTable();
        RoomViewTableModel mainViewTableModel = RoomViewTableModel.assembleModel(data);
        mainViewTable.setModel(mainViewTableModel);
        mainViewTable.randerRule();
        JScrollPane jScrollPane = new JScrollPane(mainViewTable);
        contentPane.add(jScrollPane,BorderLayout.CENTER);
    }
    public void layoutNorth(Container contentPane) {
        northPanel.add(returnBtn);
        returnBtn.addActionListener(roomHandler);  //按钮监听
        northPanel.add(exitBtn);
        exitBtn.addActionListener(roomHandler);
        northPanel.add(addBtn);
        addBtn.addActionListener(roomHandler);  //按钮监听
        northPanel.add(searchTxt);
        northPanel.add(searchBtn);
        searchBtn.addActionListener(roomHandler);
        northPanel.add(refreshBtn);
        refreshBtn.addActionListener(roomHandler);
        northPanel.add(allRCBtn);
        allRCBtn.addActionListener(roomHandler);

        contentPane.add(northPanel,BorderLayout.NORTH);
    }
    public Vector<Vector<Object>> loadTable() {  //加载读取全部数据页面
        RoomService roomService = new RoomService();
        Vector<Room> roomList = roomService.getRoomList();
        Vector<Vector<Object>> data = new Vector<>();
        TimeForMatter timeForMatter = new TimeForMatter();  //时间格式转换
        for(Room room : roomList) {  //遍历按行添加数据
            Vector<Object> rowVector = new Vector<>();
            rowVector.add(room.getRno());
            rowVector.add(room.getRlocation());
            rowVector.add(room.getRcapacity());
            rowVector.add(room.getRequipment());
            rowVector.add(timeForMatter.localDateTimeToString(room.getRStime()));
            rowVector.add(timeForMatter.localDateTimeToString(room.getREtime()));
            data.addElement(rowVector);
        }
        return data;
    }

    public JTextField getSearchTxt() {
        return searchTxt;
    }

//    public static void main(String[] args) {
//        new RoomView();
//    }
}
