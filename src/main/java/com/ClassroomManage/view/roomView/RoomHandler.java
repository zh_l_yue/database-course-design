package com.ClassroomManage.view.roomView;

import com.ClassroomManage.entity.RC;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.RoomService;
import com.ClassroomManage.view.allRCView.AllRCView;
import com.ClassroomManage.view.managerSelectView.ManagerSelectView;
import com.ClassroomManage.view.roomServiceView.AddRoomView;
import com.ClassroomManage.view.roomServiceView.SearchRoomView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import static com.ClassroomManage.ClassroomManageApplication.theTeacher;

public class RoomHandler implements ActionListener {
    public RoomView roomView;
    public RoomHandler(RoomView roomView) {this.roomView = roomView;}
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        String searchTxt = roomView.getSearchTxt().getText();  //获取文本框输入的数据
        RoomService roomService = new RoomService();
        Room searchRoom = roomService.getRoomById(searchTxt);  //按教室编号查找
        int flag = theTeacher.getFlag();
        if ("增加".equals(text)) {
            new AddRoomView();
        } else if ("查找".equals(text)) {
            if (searchRoom == null) {
                JOptionPane.showMessageDialog(roomView,"该教室编号不存在！");
            } else {
                new SearchRoomView(searchRoom,flag);
            }
        } else if ("刷新".equals(text)) {
            roomView.dispose();
            new RoomView(flag);
        } else if ("返回".equals(text)) {
            roomView.dispose();
            new ManagerSelectView();
        } else if ("退出".equals(text)) {
            roomView.dispose();
        } else if ("查看所有教室安排信息".equals(text)) {
            RCService rcService = new RCService();
            Vector<RC> rcList = rcService.getRCList();
            new AllRCView(rcList);
        }
    }
}

