package com.ClassroomManage.view.allRCView;

import com.ClassroomManage.entity.RC;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.TeacherService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class AllRCHandler implements ActionListener {
    public AllRCView allRCView;
    public AllRCHandler(AllRCView allRCView) {
        this.allRCView = allRCView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton jButton = (JButton) e.getSource();
        String text = jButton.getText();
        if ("返回".equals(text)) {
            allRCView.dispose();
        } else if ("查找".equals(text)) {
            String rno = allRCView.getSearchTxt1();
            String cname = allRCView.getSearchTxt2();
            String tname = allRCView.getSearchTxt3();
            if (rno == null && cname == null && tname == null) {
                allRCView.dispose();
                RCService rcService = new RCService();
                Vector<RC> rcList = rcService.getRCList();
                new AllRCView(rcList);
            } else {
                CourseService courseService = new CourseService();
                String cno = courseService.getCnoByCname(cname);
                TeacherService teacherService = new TeacherService();
                String tno = teacherService.getTnoByTname(tname);
                RCService rcService = new RCService();
                RC rc = new RC();
                rc.setRno(rno);
                rc.setCno(cno);
                rc.setTno(tno);
                rc.setUStime(null);
                rc.setUEtime(null);
                Vector<RC> rcList = rcService.getRCByEnter(rc);
                if (rcList.isEmpty()) {
                    JOptionPane.showMessageDialog(allRCView,"无符合条件的记录！");
                } else {
                    allRCView.dispose();
                    new AllRCView(rcList);
                }
            }
        }
    }
}
