package com.ClassroomManage.view.allRCView;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

public class AllRCTableModel extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();
    static {
        columns.addElement("教室编号");
        columns.addElement("课程名称");
        columns.addElement("教师名称");
        columns.addElement("上课时间");
        columns.addElement("下课时间");
    }
    private AllRCTableModel() {
        super(null,columns);
    }
    private static AllRCTableModel allRCTableModel = new AllRCTableModel();
    public static AllRCTableModel assembleModel(Vector<Vector<Object>> data) {
        allRCTableModel.setDataVector(data,columns);
        return allRCTableModel;
    }
    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
