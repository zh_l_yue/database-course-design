package com.ClassroomManage.view.allRCView;

import com.ClassroomManage.entity.Course;
import com.ClassroomManage.entity.RC;
import com.ClassroomManage.entity.Room;
import com.ClassroomManage.entity.Teacher;
import com.ClassroomManage.service.CourseService;
import com.ClassroomManage.service.RCService;
import com.ClassroomManage.service.RoomService;
import com.ClassroomManage.service.TeacherService;
import com.ClassroomManage.utils.TimeForMatter;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class AllRCView extends JFrame {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JButton returnBtn = new JButton("返回");
    JLabel searchLabel1 = new JLabel("教室编号");
    JComboBox searchTxt1 = new JComboBox();
    JLabel searchLabel2 = new JLabel("课程名称");
    JComboBox searchTxt2 = new JComboBox();
    JLabel searchLabel3 = new JLabel("教师名称");
    JComboBox searchTxt3 = new JComboBox();
    JButton searchBtn = new JButton("查找");
    AllRCViewTable allRCViewTable = new AllRCViewTable();
    AllRCHandler allRCHandler;
    public AllRCView(Vector<RC> rcList) {
        super("教师信息管理表");
        allRCHandler = new AllRCHandler(this);
        Container contentPane = getContentPane();
        layoutNorth(contentPane);  //增删改查按钮
        layoutCenter(contentPane,rcList);  //数据显示框

        RoomService roomService = new RoomService();
        Vector<Room> roomList = roomService.getRoomList();
        searchTxt1.addItem(null);
        for (Room room : roomList) {
            searchTxt1.addItem(room.getRno());
        }
        CourseService courseService = new CourseService();
        Vector<Course> courseList = courseService.getCourseList();
        searchTxt2.addItem(null);
        for (Course course : courseList) {
            searchTxt2.addItem(course.getCname());
        }
        TeacherService teacherService = new TeacherService();
        Vector<Teacher> teacherList = teacherService.getTeacherList();
        searchTxt3.addItem(null);
        for (Teacher teacher : teacherList) {
            searchTxt3.addItem(teacher.getTname());
        }

        searchTxt1.setPreferredSize(new Dimension(100,30));
        searchTxt2.setPreferredSize(new Dimension(100,30));
        searchTxt3.setPreferredSize(new Dimension(100,30));

        //页面设置
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        Rectangle rectangle = new Rectangle(screenInsets.left,screenInsets.top,screenSize.width-screenInsets.left-screenInsets.right,screenSize.height-screenInsets.top-screenInsets.bottom);
        setBounds(rectangle);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);
    }
    public void layoutCenter(Container contentPane,Vector<RC> rcList) {  //放入数据
        Vector<Vector<Object>> data = loadTable(rcList);
        AllRCTableModel allRCTableModel = AllRCTableModel.assembleModel(data);
        allRCViewTable.setModel(allRCTableModel);
        allRCViewTable.randerRule();
        JScrollPane jScrollPane = new JScrollPane(allRCViewTable);
        contentPane.add(jScrollPane,BorderLayout.CENTER);
    }
    public void layoutNorth(Container contentPane) {
        northPanel.add(returnBtn);
        returnBtn.addActionListener(allRCHandler);
        northPanel.add(searchLabel1);
        northPanel.add(searchTxt1);
        northPanel.add(searchLabel2);
        northPanel.add(searchTxt2);
        northPanel.add(searchLabel3);
        northPanel.add(searchTxt3);
        northPanel.add(searchBtn);
        searchBtn.addActionListener(allRCHandler);

        contentPane.add(northPanel,BorderLayout.NORTH);
    }
    public Vector<Vector<Object>> loadTable(Vector<RC> rcList) {  //加载读取全部数据页面
        Vector<Vector<Object>> data = new Vector<>();  //存放数据
        TimeForMatter timeForMatter = new TimeForMatter();  //时间格式转换
        //RCService rcService = new RCService();
        CourseService courseService = new CourseService();
        TeacherService teacherService = new TeacherService();
        for (RC rc : rcList) {  //遍历按行添加数据
            Vector<Object> rowVector = new Vector<>();
            rowVector.add(rc.getRno());
            String cno = rc.getCno();
            rowVector.add(courseService.getCourseByCno(cno).getCname());
            String tno = rc.getTno();
            rowVector.add(teacherService.getTeacherListByTno(tno).getTname());
            rowVector.add(timeForMatter.localDateTimeToString(rc.getUStime()));
            rowVector.add(timeForMatter.localDateTimeToString(rc.getUEtime()));
            data.addElement(rowVector);
        }
        return data;
    }

    public String getSearchTxt1() {
        if (searchTxt1.getSelectedItem() != null) {
            return searchTxt1.getSelectedItem().toString();
        }
        return null;
    }

    public String getSearchTxt2() {
        if (searchTxt2.getSelectedItem() != null) {
            return searchTxt2.getSelectedItem().toString();
        }
        return null;
    }

    public String getSearchTxt3() {
        if (searchTxt3.getSelectedItem() != null) {
            return searchTxt3.getSelectedItem().toString();
        }
        return null;
    }
}
